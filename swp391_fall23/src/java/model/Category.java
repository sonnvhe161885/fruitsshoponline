/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class Category {

    private int cid;
    private String cName;
    private boolean cStatus;

    public Category() {
    }

    public Category(int cid, String cName, boolean cStatus) {
        this.cid = cid;
        this.cName = cName;
        this.cStatus = cStatus;
    }

    public boolean iscStatus() {
        return cStatus;
    }

    public void setcStatus(boolean cStatus) {
        this.cStatus = cStatus;
    }

    

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

}
