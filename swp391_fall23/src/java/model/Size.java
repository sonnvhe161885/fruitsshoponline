/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class Size {

    private int sid;
    private String sName;
    private int quantity;
    private double price;

    public Size() {
    }

    public Size(int sid, String sName) {
        this.sid = sid;
        this.sName = sName;
    }

    public Size(int sid, String sName, int quantity, double price) {
        this.sid = sid;
        this.sName = sName;
        this.quantity = quantity;
        this.price = price;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    
}
