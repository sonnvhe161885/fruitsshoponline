package model;

import dto.StoresDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Store {

    private Integer stoid;
    private String stoName;
    private String stoAddress;
    private String stoPhone;
    private String stoTimeOpen;
    private String stoTimeClosed;
    private Status status;
    private Boolean deleted;

    public Store(int stoid, String stoName, String stoAddress, String stoPhone, String stoTimeOpen, String stoTimeClosed, Status status) {
        this.stoid = stoid;
        this.stoName = stoName;
        this.stoAddress = stoAddress;
        this.stoPhone = stoPhone;
        this.stoTimeOpen = stoTimeOpen;
        this.stoTimeClosed = stoTimeClosed;
        this.status = status;
    }

    public static Store toModel(StoresDTO dto) {
        if (dto == null) {
            return null;
        }
        Store model = new Store();
        model.setStoid(dto.getStoId());
        model.setStoName(dto.getStoName());
        model.setStoPhone(dto.getStoPhone());
        model.setStoAddress(dto.getStoAddress());
        model.setStoTimeOpen(dto.getStoTimeOpen());
        model.setStoTimeClosed(dto.getStoTimeClose());
        model.setStatus(new Status(dto.getStatusId(), dto.getStatusName()));

        return model;
    }

}
