/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package model;

import java.util.List;
import lombok.Builder;

@Builder
public class Product {
    private int pid;
    private String pName;
    private String pDetail;
    private String pBrand;
    private String pSKU;
    
    private Category category;
    private Status status;
    private Store store;
    private double price;
    private double priceIn;
    private float quantity;
    private Image image;
    private boolean isOutstanding; 

    public Product() {
    }

    public Product(int pid, String pName, String pDetail, String pBrand, String pSKU, Category category, Status status, Store store, double price, double priceIn, float quantity, Image image, boolean isOutstanding) {
        this.pid = pid;
        this.pName = pName;
        this.pDetail = pDetail;
        this.pBrand = pBrand;
        this.pSKU = pSKU;
        this.category = category;
        this.status = status;
        this.store = store;
        this.price = price;
        this.priceIn = priceIn;
        this.quantity = quantity;
        this.image = image;
        this.isOutstanding = isOutstanding;
    }
    
    

    public Product(int pid, String pName, String pDetail, String pBrand, String pSKU, Category category, Status status, Store store, double price, double priceIn, float quantity, Image image) {
        this.pid = pid;
        this.pName = pName;
        this.pDetail = pDetail;
        this.pBrand = pBrand;
        this.pSKU = pSKU;
        this.category = category;
        this.status = status;
        this.store = store;
        this.price = price;
        this.priceIn = priceIn;
        this.quantity = quantity;
        this.image = image;
    }

    public Product(int pid, String pName, String pDetail, String pBrand, String pSKU, Category category, Status status, Store store, double price, float quantity, Image image) {
        this.pid = pid;
        this.pName = pName;
        this.pDetail = pDetail;
        this.pBrand = pBrand;
        this.pSKU = pSKU;
        this.category = category;
        this.status = status;
        this.store = store;
        this.price = price;
        this.quantity = quantity;
        this.image = image;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpDetail() {
        return pDetail;
    }

    public void setpDetail(String pDetail) {
        this.pDetail = pDetail;
    }

    public String getpBrand() {
        return pBrand;
    }

    public void setpBrand(String pBrand) {
        this.pBrand = pBrand;
    }

    public String getpSKU() {
        return pSKU;
    }

    public void setpSKU(String pSKU) {
        this.pSKU = pSKU;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPriceIn() {
        return priceIn;
    }

    public void setPriceIn(double priceIn) {
        this.priceIn = priceIn;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public boolean isIsOutstanding() {
        return isOutstanding;
    }

    public void setIsOutstanding(boolean isOutstanding) {
        this.isOutstanding = isOutstanding;
    }

    

}
