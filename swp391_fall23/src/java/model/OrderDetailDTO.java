/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author sonng
 */
public class OrderDetailDTO {
    private int orderDetailId;
    private int quantity;
    private int productSizeId;
    private int productId;
    private int orderId;
    private Product product;
    
    public OrderDetailDTO() {
    }

    public OrderDetailDTO(int orderDetailId, int quantity, int productSizeId, int productId, int orderId) {
        this.orderDetailId = orderDetailId;
        this.quantity = quantity;
        this.productSizeId = productSizeId;
        this.productId = productId;
        this.orderId = orderId;
    }

    public int getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getProductSizeId() {
        return productSizeId;
    }

    public void setProductSizeId(int productSizeId) {
        this.productSizeId = productSizeId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    
}
