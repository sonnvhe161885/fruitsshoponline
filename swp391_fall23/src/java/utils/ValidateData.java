/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Admin
 */
public class ValidateData {
    
    public static boolean isValidName(String name) {
        // Regex chỉ cho phép các ký tự chữ cái và số
        return name != null && !name.isEmpty() && Pattern.matches("^[\\p{L}a-zA-Z]+( [\\p{L}a-zA-Z]+)*$", name);
    }
    
    public static boolean isValidEmail(String email) {
        String emailPattern = "^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$";
        return email.matches(emailPattern);
    }
    
//     public static void main(String[] args) {
//        String hoTen = "huy vũ  Đanh"; // Thay thế chuỗi này bằng họ tên cần kiểm tra
//
//        // Biểu thức chính quy
//        boolean b = isValidName(hoTen);
//         System.out.println(b);
    }






