/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import java.security.SecureRandom;

/**
 *
 * @author Trung Kien
 */
public class StringUtils {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    public static boolean isBlank(String text) {
        return text == null || "".equals(text);
    }

    public static String genOrderCode() {
        int r = SECURE_RANDOM.nextInt(99999999);
        return String.valueOf(r);
    }

}
