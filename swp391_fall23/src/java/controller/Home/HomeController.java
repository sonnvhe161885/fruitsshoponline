/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Home;

import dal.BannerDAO;
import dal.ProductDAO;
import dal.SliderDAO;
import dto.SliderDTO;
import dto.StoresDTO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import model.Banner;
import model.Product;
import service.SliderService;
import service.StoreService;
import service.interfaces.ISliderService;
import service.interfaces.IStoreService;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "Home", value = {"/home", ""})
public class HomeController extends HttpServlet {

    private final IStoreService iStoreService = new StoreService();
    private final ISliderService iSliderService = new SliderService();
    private final ProductDAO productDAO = new ProductDAO();
    private int stiId;
    

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        Cookie[] cookies = req.getCookies();
        String selectedCity = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("selectedCity".equals(cookie.getName())) {
                    selectedCity = cookie.getValue();
                    break;
                }
            }
        if(selectedCity == null){
            stiId = 1;
//            Cookie newSelectedCityCookie = new Cookie("selectedCity", "3");
//            resp.addCookie(newSelectedCityCookie);
        }else{
            stiId = Integer.parseInt(selectedCity);
        }
         List<StoresDTO> storesDTOs = iStoreService.getListStores();

            req.setAttribute("listStores", storesDTOs);
        List<SliderDTO> sliderDTOs = iSliderService.getAll();
        List<Product> products = productDAO.getProductOutstanding(1,stiId);
        
        BannerDAO d = new BannerDAO();
        Banner b = d.getBannerActive();
        req.setAttribute("b", b);

        req.setAttribute("sliders", sliderDTOs);
        req.setAttribute("products", products);

        RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
        rd.forward(req, resp);
    }
    }

}
