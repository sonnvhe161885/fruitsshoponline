/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Shop;

import dal.ProductDAO;
import dto.StoresDTO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Product;
import service.StoreService;
import service.interfaces.IStoreService;
import utils.Helpers;

@WebServlet(name = "ShopController", urlPatterns = {"/shop"})
public class ShopController extends HttpServlet {

    private final IStoreService iStoreService = new StoreService();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {

            Cookie[] cookies = request.getCookies();
            String selectedCity = null;
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if ("selectedCity".equals(cookie.getName())) {
                        selectedCity = cookie.getValue();
                        break;
                    }
                }
                if (selectedCity == null) {
                    selectedCity = "3";
                }
                int stiId = Integer.parseInt(selectedCity);

                List<StoresDTO> storesDTOs = iStoreService.getListStores();

                request.setAttribute("listStores", storesDTOs);
                //get request param
                String xpage = request.getParameter("page");
                String xnumberPerPage = request.getParameter("numberPerPage");
                ProductDAO pdao = new ProductDAO();

                int cid = Helpers.parseInt(request.getParameter("cid"), 0);

                String price_from = request.getParameter("price_from");
                String price_to = request.getParameter("price_to");

                String search = request.getParameter("search");

                int sortBy = Helpers.parseInt(request.getParameter("sortBy"), 0);

                List<Product> data = pdao.getProduct(cid, price_from, price_to, search, sortBy, stiId);

                // pagination
                int size = data.size();
                int page = Helpers.parseInt(xpage, 1);
                int numberPerPage = Helpers.parseInt(xnumberPerPage, 6);

                int numberOfPage = ((size % numberPerPage == 0) ? (size / numberPerPage) : (size / numberPerPage + 1));
                int start = (page - 1) * numberPerPage;
                int end = Math.min(page * numberPerPage, size);

                List<Product> last = pdao.pagination(data, start, end);

                request.setAttribute("data", last);
                request.setAttribute("page", page);
                request.setAttribute("numberPerPage", numberPerPage);
                request.setAttribute("numberOfPage", numberOfPage);
                request.setAttribute("size", size);
                request.setAttribute("start", start);
                request.setAttribute("end", end);

                request.setAttribute("cid", cid);
                request.setAttribute("price_from", price_from);
                request.setAttribute("price_to", price_to);
                request.setAttribute("search", search);
                request.setAttribute("sortBy", sortBy);

                request.getRequestDispatcher("shop/shop.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
