/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Account;

import dal.UserDAO;
import dto.StoresDTO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.websocket.Session;
import java.util.List;
import model.User;
import service.StoreService;
import service.interfaces.IStoreService;

/**
 *
 * @author Admin
 */
public class ChangePass_Controller extends HttpServlet {
private final IStoreService iStoreService = new StoreService();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Changepass_Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Changepass_Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         List<StoresDTO> storesDTOs = iStoreService.getListStores();
        request.setAttribute("listStores", storesDTOs);
        request.getRequestDispatcher("login/changepass.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String oldpass = request.getParameter("oldpass");
        String newpass = request.getParameter("newpass");
        String newpass1 = request.getParameter("newpass1");
        
        HttpSession session = request.getSession();
        User u = (User)session.getAttribute("account");
        UserDAO d = new UserDAO();
        String msg = "";
        
        if(d.convertPassToMD5(oldpass).equals(u.getPass())){
            if(newpass.equals(newpass1)){
                if(oldpass.equals(newpass)){
                    msg = "Mật khẩu cũ không được giống mật khẩu mới";
                    request.setAttribute("msg", msg);
                    request.getRequestDispatcher("login/changepass.jsp").forward(request, response);
                }
                else{
                    u.setPass(newpass);
                    if(d.UpdateUser(u,1)){
                    response.sendRedirect("profile");
                    }else{
                        msg = "Có lỗi xảy ra";
                        request.setAttribute("msg", msg);
                        request.getRequestDispatcher("login/changepass.jsp").forward(request, response);
                    }
                }
            }else{
                msg = "Mật khẩu không khớp nhau";
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("login/changepass.jsp").forward(request, response);

            }
        }else{
            msg = "Mật khẩu không đúng";
            request.setAttribute("msg", msg);
            request.getRequestDispatcher("login/changepass.jsp").forward(request, response);

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
