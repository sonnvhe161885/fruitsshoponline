/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Account;

import dal.AddressDAO;
import dto.StoresDTO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Address;
import model.User;
import service.StoreService;
import service.interfaces.IStoreService;

/**
 *
 * @author Admin
 */
public class UserAddress_Controller extends HttpServlet {
private final IStoreService iStoreService = new StoreService();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserAddress_Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserAddress_Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<StoresDTO> storesDTOs = iStoreService.getListStores();
        request.setAttribute("listStores", storesDTOs);
        HttpSession session = request.getSession();
        User u = (User)session.getAttribute("account");
        AddressDAO d = new AddressDAO();
        List<Address> l = d.getAddressByUid(u.getUserId());

        request.setAttribute("a", l);
        request.getRequestDispatcher("login/useraddress.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String defaut = request.getParameter("default");
        String city = request.getParameter("cityText");
        String district = request.getParameter("districtText");
        String ward = request.getParameter("wardText");
        boolean b = true;
        String msg;    
        AddressDAO ad = new AddressDAO();
        AddressDAO d = new AddressDAO();
        
        List<StoresDTO> storesDTOs = iStoreService.getListStores();
        request.setAttribute("listStores", storesDTOs);
        
        HttpSession session = request.getSession();
        User u = (User)session.getAttribute("account");
        
        AddressDAO dd = new AddressDAO();
        List<Address> l = dd.getAddressByUid(u.getUserId());

        request.setAttribute("a", l);
        
        
            if(d.addressCount(u.getUserId()) >= 3){
                msg = "Bạn chỉ được thêm tối đa 3 địa chỉ!";
                request.setAttribute("msg", msg);
                request.setAttribute("a", l);
                request.getRequestDispatcher("login/useraddress.jsp").forward(request, response);
                return;
        }
        
            if(!utils.ValidateData.isValidName(name)){
                msg = "Tên không hợp lệ, vui lòng nhập lại";
                request.setAttribute("msg", msg);
                request.setAttribute("a", l);
                request.getRequestDispatcher("login/useraddress.jsp").forward(request, response);
                return;
            }

            if(defaut == null){
                b = false;
            }
        
            if(b == true){
                ad.setDefault(u.getUserId());
            }
        
            Address a = new Address(0, u.getUserId(), name, address, phone, ward, district, city, b, true);

            ad.addNewAddress(a);
        
            response.sendRedirect("useraddress");
        
        }
        
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
