/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Shopping;

import dal.AddressDAO;
import dal.ProductDAO;
import dal.ProductSizeDAO;
import dto.ProductDisplayCartDTO;
import dto.StoresDTO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Address;
import model.User;
import service.StoreService;
import service.interfaces.IStoreService;
import utils.JsonService;
import utils.StringUtils;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "CheckoutCartController", urlPatterns = {"/checkout-cart"})
public class CheckoutCartController extends HttpServlet {

    private final ProductDAO productDao = new ProductDAO();
    private final ProductSizeDAO productSizeDAO = new ProductSizeDAO();
    private final IStoreService iStoreService = new StoreService();
    private final AddressDAO AddressDAO = new AddressDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        List<StoresDTO> storesDTOs = iStoreService.getListStores();

        req.setAttribute("listStores", storesDTOs);

        try {
            processingCart(req, resp);
        } catch (SQLException ex) {
            Logger.getLogger(CheckoutCartController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void processingCart(HttpServletRequest req, HttpServletResponse resp)
            throws SQLException, IOException, ServletException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        Cookie[] cookies = req.getCookies();
        List<ProductDisplayCartDTO> productDisplayCartDTOList = new ArrayList<>();
        HttpSession session = req.getSession();
        User userAccountDTO = (User) session.getAttribute("account");
        if (userAccountDTO == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }
        BigDecimal total = new BigDecimal(0);
        productDisplayCartDTOList = new ArrayList<>();
        int storeId = -1;
        boolean exist = false;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("products")) {
                exist = true;
                if (!StringUtils.isBlank(cookie.getValue())) {
                    try {
                        String json = JsonService.base64ToString(cookie.getValue());
                        productDisplayCartDTOList.addAll(JsonService.parseJsonToList(json, ProductDisplayCartDTO.class));
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(CartController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
            }
        }
        if (productDisplayCartDTOList.isEmpty()) {
            resp.sendRedirect(req.getContextPath() + "/shop");
            return;
        }
        for (int i = 0; i < productDisplayCartDTOList.size(); i++) {
            float quantity = 1;
            for (int j = i + 1; j < productDisplayCartDTOList.size(); j++) {
                if (productDisplayCartDTOList.get(i).getPid() == productDisplayCartDTOList.get(j).getPid()) {
                    quantity++;
                    productDisplayCartDTOList.remove(j);
                    j--;
                    productDisplayCartDTOList.get(i).setQuantity(quantity);
                }
            }
        }
        double sum = 0;
        for (ProductDisplayCartDTO productDisplayCartDTO : productDisplayCartDTOList) {
            double totalOnProduct = productDisplayCartDTO.getPrice() * productDisplayCartDTO.getQuantity();
            productDisplayCartDTO.setTotal(totalOnProduct);
            sum += totalOnProduct;
            storeId = productDisplayCartDTO.getStore().getStoid();
        }
        StoresDTO storesDTO = iStoreService.getById(storeId);
        List<Address> addresses = AddressDAO.getAddressByUid(userAccountDTO.getUserId());
        Address addressDefault = getAddressDefault(addresses);
        req.setAttribute("addresses", addresses);
        req.setAttribute("addressDefault", addressDefault);
        req.setAttribute("store", storesDTO);
        req.setAttribute("productDisplayCartDTOList", productDisplayCartDTOList);
        req.setAttribute("total", formatDouble(sum));
        session.setAttribute("account", userAccountDTO);

        RequestDispatcher rd = req.getRequestDispatcher("checkoutCart.jsp");
        rd.forward(req, resp);
    }

    private Address getAddressDefault(List<Address> addresses) {
        for (Address a : addresses) {
            if (a.isDefaut()) {
                addresses.remove(a);
                return a;
            }
        }
        return null;
    }

    public double formatDouble(double in) {
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        return Double.parseDouble(decimalFormat.format(in));
    }
}
