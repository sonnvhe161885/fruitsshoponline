/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Shopping;

import dal.ProductDAO;
import dal.ProductSizeDAO;
import dto.ProductDisplayCartDTO;
import dto.StoresDTO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
import model.ProductSize;
import model.User;
import service.StoreService;
import service.interfaces.IStoreService;
import utils.JsonService;
import utils.StringUtils;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "ShoppingCard", value = "/shopping-cart")
public class CartController extends HttpServlet {

    private final ProductDAO productDao = new ProductDAO();
    private final ProductSizeDAO productSizeDAO = new ProductSizeDAO();
    private final IStoreService iStoreService = new StoreService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");

        List<StoresDTO> storesDTOs = iStoreService.getListStores();

        req.setAttribute("listStores", storesDTOs);
        String path = req.getServletPath();
        try {
            switch (path) {
                case "/shopping-cart/clear":
//                    clearCart(req, resp);
                    break;
                case "/cart/editAll":
//                    editAllProductInCart(req, resp);
                    break;
                default:
                    processingCart(req, resp);
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void increaseQuantityProduct(int productId, int quantity) {
        Product product = productDao.getProductById(productId);
        product.setQuantity(product.getQuantity() + quantity);
        productDao.update(product);
    }

    private void decreaseQuantityProduct(int productId, int quantity) {
        Product product = productDao.getProductById(productId);
        product.setQuantity(product.getQuantity() - quantity);
        productDao.update(product);
    }

    private void processingCart(HttpServletRequest req, HttpServletResponse resp)
            throws SQLException, IOException, ServletException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        Cookie[] cookies = req.getCookies();
        List<ProductDisplayCartDTO> productDisplayCartDTOList = new ArrayList<>();
        HttpSession session = req.getSession();
        User userAccountDTO = (User) session.getAttribute("account");
        if (userAccountDTO == null) {
            String context = req.getContextPath();
            resp.sendRedirect(context + "/login");
            return;
        }
        Cookie cookieTotalPrice = null;
        if (req.getAttribute("productDisplayCartDTOList") == null) {
            boolean exist = false;
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("products")) {
                    exist = true;
                    if (!StringUtils.isBlank(cookie.getValue())) {
                        try {
                            String json = JsonService.base64ToString(cookie.getValue());
                            productDisplayCartDTOList.addAll(JsonService.parseJsonToList(json, ProductDisplayCartDTO.class));
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(CartController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                }

                if (cookie.getName().equals("totalPrice")) {
                    cookieTotalPrice = cookie;
                }
            }
            // if cookie doesn't exist, then redirect user to empty-cart
            if (exist == false) {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("cart.jsp");
                requestDispatcher.forward(req, resp);
                return;
            }
        } else {
            productDisplayCartDTOList = (List<ProductDisplayCartDTO>) req.getAttribute("productDisplayCartDTOList");
        }
        double sum = 0;
        for (ProductDisplayCartDTO productDisplayCartDTO : productDisplayCartDTOList) {
            double totalOnProduct = productDisplayCartDTO.getPrice() * productDisplayCartDTO.getQuantity();
            productDisplayCartDTO.setTotal(totalOnProduct);
            sum += totalOnProduct;
        }
        req.setAttribute("productDisplayCartDTOList", productDisplayCartDTOList);
        req.setAttribute("total", formatDouble(sum));

        cookieTotalPrice = cookieTotalPrice != null ? cookieTotalPrice : new Cookie("totalPrice", String.valueOf(formatDouble(sum)));
        cookieTotalPrice.setHttpOnly(true);
        cookieTotalPrice.setMaxAge(60 * 60 * 24);
        cookieTotalPrice.setPath("/");
        cookieTotalPrice.setValue(String.valueOf(formatDouble(sum)));
        resp.addCookie(cookieTotalPrice);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("cart.jsp");
        requestDispatcher.forward(req, resp);
    }

    public double formatDouble(double in) {
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        return Double.parseDouble(decimalFormat.format(in));
    }
}
