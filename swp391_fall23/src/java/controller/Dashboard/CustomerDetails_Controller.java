/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Dashboard;

import dal.OrderDAO;
import dal.StoreDAO;
import dal.UserDAO;
import dto.UserDTO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;
import model.Order;
import model.Store;
import model.User;

/**
 *
 * @author Admin
 */
public class CustomerDetails_Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CustomerDetails_Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CustomerDetails_Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("userId");
        int uid = Integer.parseInt(id);
        UserDAO u = new UserDAO();
        OrderDAO o = new OrderDAO();
        User user = u.getUserById(uid);
        
        int y, m;
        String year = request.getParameter("year");
        String month = request.getParameter("month");

        LocalDate currentDate = LocalDate.now();
        int currentMonth = currentDate.getMonthValue();
        int currentYear = currentDate.getYear();

        if (year == null && month == null) {
            y = currentYear;
            m = currentMonth;
        } else {
            y = Integer.parseInt(year);
            m = Integer.parseInt(month);
        }
        
        UserDTO ud = o.getTotalOrderCustomer(uid, y, m);
        if(ud == null){
            ud = new UserDTO(uid, 0, 0, 0);
        }
        OrderDAO od = new OrderDAO();
        List<Order> order = od.getOderById(uid);
        request.setAttribute("order", order);      
        request.setAttribute("ud", ud);
        request.setAttribute("m", m);
        request.setAttribute("y", y);
        request.setAttribute("u", user);
        request.getRequestDispatcher("user/CustomerDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
