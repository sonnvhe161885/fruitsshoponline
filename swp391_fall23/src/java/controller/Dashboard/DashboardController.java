/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Dashboard;

import dal.OrderDAO;
import dal.ProductDAO;
import dal.UserDAO;
import dto.ReportRevenueDTO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import model.Order;
import model.User;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "DashboardController", value = {"/dashboard"})
public class DashboardController extends HttpServlet {

    private final UserDAO userDao = new UserDAO();
    private final ProductDAO productDao = new ProductDAO();
    private final OrderDAO orderDao = new OrderDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int totalUser = userDao.countAllUser();
        int totalProduct = productDao.countAll();
        int totalOrder = orderDao.countAll();
        int totalProductOutOfStock = productDao.countAllByTotal(10);

        List<User> users = userDao.findNewUsers(5);
        List<Order> orders = orderDao.getNewOrders(5);
        ReportRevenueDTO revenueDTO = orderDao.getReportRevenue();
        revenueDTO.formatRevenue();

        req.setAttribute("totalUser", totalUser);
        req.setAttribute("totalProduct", totalProduct);
        req.setAttribute("totalOrder", totalOrder);
        req.setAttribute("totalPrice", revenueDTO.getRevenueOut());
        req.setAttribute("totalPriceIn", revenueDTO.getRevenueIn());
        req.setAttribute("totalProductOutOfStock", totalProductOutOfStock);

        req.setAttribute("users", users);
        req.setAttribute("orders", orders);

        RequestDispatcher rs = req.getRequestDispatcher("dashboard.jsp");
        rs.forward(req, resp);
    }

}
