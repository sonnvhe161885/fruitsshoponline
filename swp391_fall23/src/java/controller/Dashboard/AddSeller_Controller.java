/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Dashboard;

import dal.StoreDAO;
import dal.UserDAO;
import model.Role;
import model.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Store;
import utils.SendMail;

/**
 *
 * @author Admin
 */
public class AddSeller_Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.getRequestDispatcher("/user/AddSeller.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StoreDAO d = new StoreDAO();
        List<Store> s = d.getAll();
        request.setAttribute("s", s);
        request.getRequestDispatcher("/user/AddSeller.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = request.getParameter("userName");
        String email = request.getParameter("email");
        String pass = SendMail.generateRandomPassword();
        String phone = request.getParameter("phone");
        String storeId = request.getParameter("selectedOption");
        int store = Integer.parseInt(storeId);

        UserDAO d = new UserDAO();
        StoreDAO st = new StoreDAO();

        boolean exists = d.isEmailExists(email);
        String msg = "";

        if (!utils.ValidateData.isValidName(userName)) {
            msg = "Tên không hợp lệ, vui lòng nhập lại";
            request.setAttribute("msg", msg);
            request.setAttribute("phone", phone);
            request.setAttribute("email", email);
            List<Store> sto = st.getAll();
            request.setAttribute("s", sto);
            request.getRequestDispatcher("/user/AddSeller.jsp").forward(request, response);
            return;
        }

        if (!utils.ValidateData.isValidEmail(email)) {
            msg = "Email không hợp lệ, vui lòng nhập lại";
            request.setAttribute("msg", msg);
            request.setAttribute("name", userName);
            request.setAttribute("phone", phone);
            List<Store> sto = st.getAll();
            request.setAttribute("s", sto);
            request.getRequestDispatcher("/user/AddSeller.jsp").forward(request, response);
            return;
        }
        if (exists) {
            msg = "Email đã tồn tại vui lòng kiểm tra lại";
            request.setAttribute("msg", msg);
            request.setAttribute("name", userName);
            request.setAttribute("phone", phone);
            List<Store> sto = st.getAll();
            request.setAttribute("s", sto);
            request.getRequestDispatcher("/user/AddSeller.jsp").forward(request, response);
            return;

        }
        if(userName != null && email != null && phone != null && storeId !=null){
            Store s = st.getStoreById(store);
            Role role = d.getRoleById(2);
            d.addUser(new User(-1, userName, email, phone, pass, role, s, true));
            SendMail.sendMail(email, 2, pass);
            msg = "Thêm mới thành công";
            List<Store> sto = st.getAll();
            request.setAttribute("s", sto);
            request.setAttribute("msg", msg);
            request.getRequestDispatcher("/user/AddSeller.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
