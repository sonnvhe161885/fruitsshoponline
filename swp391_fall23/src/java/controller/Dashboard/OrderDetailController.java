/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Dashboard;

import dal.OrderDAO;
import dal.OrderDetailDAO;
import dal.StatusDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import model.Order;
import model.OrderDetail;
import model.Role;
import model.Status;
import model.Store;
import model.User;
import utils.StringUtils;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "OrderDetailController", value = {"/order-detail"})
public class OrderDetailController extends HttpServlet {

    private final OrderDetailDAO orderDetailDAO = new OrderDetailDAO();
    private final OrderDAO orderDAO = new OrderDAO();
    private final StatusDAO statusDao = new StatusDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = this.getCurrentUser(req, resp);
        if (user == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }
        String orderCode = req.getParameter("orderCode");
        String action = req.getParameter("action");

        Order order = orderDAO.getByOrderCode(orderCode);
        List<OrderDetail> orderDetails = orderDetailDAO.findAllByOrderCode(order.getOrderId());
        req.setAttribute("order", order);
        if (!StringUtils.isBlank(action)) {
            List<Status> statuses = new ArrayList<>();
            statuses.add(statusDao.getByName("Waiting_Confirm"));
            statuses.add(statusDao.getByName("Confirm"));
            req.setAttribute("action", action);
            req.setAttribute("statuses", statuses);

        }
        req.setAttribute("orderDetails", orderDetails);

        RequestDispatcher rs = req.getRequestDispatcher("orderDetail.jsp");
        rs.forward(req, resp);
    }

    private User getCurrentUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("account");
        return user;
    }

}
