/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Dashboard;

import dal.OrderDAO;
import dal.StatusDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import model.Order;
import model.Role;
import model.Status;
import model.Store;
import model.User;
import utils.MessageEnum;
import utils.MessageUtils;
import utils.StringUtils;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "ManagerOrdersSellerController", value = {"/manager-orders-seller", "/manager-orders-seller/update-status"})
public class ManageOrdersSeller extends HttpServlet {

    private final OrderDAO orderDao = new OrderDAO();
    private final StatusDAO statusDao = new StatusDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = this.getCurrentUser(req, resp);
        if (user == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }
        String uri = req.getRequestURI();
        if (uri.contains("/manager-orders-seller/update-status")) {
            String statusId = req.getParameter("statusId");
            String orderCode = req.getParameter("orderCode");
            boolean isUpdate = orderDao.update(orderCode, Integer.parseInt(statusId), user.getUserId());
            MessageEnum messageEnum = null;
            if (isUpdate) {
                messageEnum = MessageEnum.UPDATE_STATUS_ORDER_SUCCESS;
            } else {
                messageEnum = MessageEnum.UPDATE_STATUS_ORDER_ERROR;
            }

            MessageUtils.showMessage(req, messageEnum);
            resp.sendRedirect(req.getContextPath() + "/manager-orders-seller");
        } else {
            resp.sendRedirect(req.getContextPath() + "/manager-orders-seller");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = this.getCurrentUser(req, resp);
        if (user == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }

        String status = req.getParameter("status");
        List<Order> orders = orderDao.findAllBySeller(user.getUserId(), StringUtils.isBlank(status) ? null : status);
        List<Status> statuses = new ArrayList<>();
        statuses.add(statusDao.getByName("Confirm"));
        statuses.add(statusDao.getByName("Preparing"));
        statuses.add(statusDao.getByName("Delivery"));
        statuses.add(statusDao.getByName("Delivery_Success"));
        statuses.add(statusDao.getByName("Order_Cancel"));

        req.setAttribute("orders", orders);
        req.setAttribute("statuses", statuses);

        RequestDispatcher rs = req.getRequestDispatcher("manageOrdersSeller.jsp");
        rs.forward(req, resp);
    }

    private User getCurrentUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("account");
        return user;
    }

}
