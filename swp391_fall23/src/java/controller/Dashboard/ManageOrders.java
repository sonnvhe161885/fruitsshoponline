/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Dashboard;

import dal.OrderDAO;
import dal.StatusDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import model.Order;
import model.Role;
import model.Status;
import model.Store;
import model.User;
import utils.MessageEnum;
import utils.MessageUtils;
import utils.StringUtils;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "ManagerOrdersController", value = {"/manager-orders", "/manager-orders/update-status"})
public class ManageOrders extends HttpServlet {

    private final OrderDAO orderDao = new OrderDAO();
    private final StatusDAO statusDao = new StatusDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = this.getCurrentUser(req, resp);
        if (user == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }
        String uri = req.getRequestURI();
        if (uri.contains("/manager-orders/update-status")) {
            String statusId = req.getParameter("statusId");
            String orderCode = req.getParameter("orderCode");
            Date deliveryDate = Date.valueOf(req.getParameter("deliveryDate"));
            boolean isUpdate = orderDao.update(orderCode, Integer.parseInt(statusId), user.getUserId(), deliveryDate);
            MessageEnum messageEnum = null;
            if (isUpdate) {
                messageEnum = MessageEnum.UPDATE_STATUS_ORDER_SUCCESS;
            } else {
                messageEnum = MessageEnum.UPDATE_STATUS_ORDER_ERROR;
            }

            MessageUtils.showMessage(req, messageEnum);
            resp.sendRedirect(req.getContextPath() + "/manager-orders?alert=" + messageEnum.getAlert() + "&message=" + messageEnum.getMessage());
        } else {
            resp.sendRedirect(req.getContextPath() + "/manager-orders");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = this.getCurrentUser(req, resp);
        if (user == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }

        int stoId = user.getStore() == null ? -1 : user.getStore().getStoid();
        List<Order> orders;
        if ("Admin".equals(user.getRole().getRoleName())) {
            String status = req.getParameter("status");
            orders = orderDao.findAllForAdmin(StringUtils.isBlank(status) ? null : status);
        } else {
            orders = orderDao.findAll(stoId);
        }
        List<Status> statuses = new ArrayList<>();
        statuses.add(statusDao.getByName("Waiting_Confirm"));
        statuses.add(statusDao.getByName("Confirm"));

        req.setAttribute("statusFilter", statusDao.getAllByOrder());
        req.setAttribute("orders", orders);
        req.setAttribute("statuses", statuses);
        req.setAttribute("alert", req.getParameter("alert"));
        req.setAttribute("message", req.getParameter("message"));

        RequestDispatcher rs = req.getRequestDispatcher("manageOrders.jsp");
        rs.forward(req, resp);
    }

    private User getCurrentUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("account");
        // comment 90-95
//        Store store = new Store();
//        store.setStoid(user.getStore().getStoid());
//        user = user == null ? new User() : user;
//        user.setUserId(35);
//        user.setRole(new Role(2, "Nhân viên"));
//        user.setRole(new Role(3, "Admin"));
//        user.setStore(store);
        return user;
    }

}
