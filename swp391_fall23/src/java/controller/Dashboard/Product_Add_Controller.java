/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Dashboard;

import dal.ProductDAO;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import model.Category;
import model.Image;
import model.Product;
import model.Size;
import model.Status;
import model.Store;
import model.User;
import utils.Helpers;

@MultipartConfig
public class Product_Add_Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Product_Add_Controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Product_Add_Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("product/add.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pName = request.getParameter("pName");
        String pBrand = request.getParameter("pBrand");
        String pDetail = request.getParameter("pDetail");
        String pSKU = request.getParameter("pSKU");
        float quantity = Helpers.parseFloat(request.getParameter("quantity"), 0.0f);
        double price = Helpers.parseDouble(request.getParameter("price"), 0.0);
        double priceIn = Helpers.parseDouble(request.getParameter("priceIn"), 0.0);
        int cid = Helpers.parseInt(request.getParameter("cid"), 0);
        int stid = Helpers.parseInt(request.getParameter("stid"), 0);
        int stoid = Helpers.parseInt(request.getParameter("stoid"), 0);
        String msg = "";
        
        HttpSession session = request.getSession();
        User u = (User)session.getAttribute("account");

        ProductDAO pdao = new ProductDAO();

        Category c = new Category(cid, "", true);

        Status st = new Status(stid, "");

        Store sto = new Store(stoid, "", "", "", "", "", st);

        Image image = new Image(-1, "");

        // image 
        //Lay thong tin tu form voi ten la:ImageUpload
        Part filePart = request.getPart("ImageUpload");

        String realpart = request.getServletContext().getRealPath("/img/product");
        realpart = realpart.replace("\\build\\web", "\\web");
        if (filePart != null) {
            String filename = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            filePart.write(realpart + "/" + filename);
            image.setiUrl(filename);
            
            Product p = new Product(-1, pName, pDetail, pBrand, pSKU, c, st, u.getStore(), price, priceIn, quantity, image, false);
            
            boolean pp = pdao.add(p);
            p.setPid(pdao.getLastProductID());
            pdao.addImage(p);
            if (pp == true) {
                msg = "Thêm mới thành công";
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("product/add.jsp").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
