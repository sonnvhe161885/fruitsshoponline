/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dal.StoreDAO;
import dto.StoresDTO;
import java.util.ArrayList;
import java.util.List;
import model.Store;
import service.interfaces.IStoreService;

/**
 *
 * @author Trung Kien
 */
public class StoreService implements IStoreService {

    private final StoreDAO storeDAO = new StoreDAO();

    @Override
    public boolean insertStore(StoresDTO storeDTO) {
        Store store = Store.toModel(storeDTO);

        return storeDAO.create(store);
    }

    @Override
    public List<StoresDTO> getListStores() {
        List<StoresDTO> list = new ArrayList<>();
        List<Store> storeses = storeDAO.findAll();
        for (Store stores : storeses) {
            list.add(StoresDTO.toDto(stores));
        }
        return list;
    }

    @Override
    public StoresDTO getById(int stoId) {
        Store store = storeDAO.getById(stoId);
        return StoresDTO.toDto(store);
    }

    @Override
    public boolean updateStore(StoresDTO storesDTO) {
        Store store = Store.toModel(storesDTO);
        return storeDAO.update(store);
    }

    @Override
    public boolean deleteById(int stoId) {
        return storeDAO.deleteById(stoId);
    }
}
