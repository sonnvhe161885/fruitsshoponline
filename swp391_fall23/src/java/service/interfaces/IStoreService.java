/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service.interfaces;

import dto.StoresDTO;
import java.util.List;

/**
 *
 * @author Trung Kien
 */
public interface IStoreService {
    boolean insertStore(StoresDTO storeDTO);
    List<StoresDTO> getListStores();
    StoresDTO getById(int stoId);
    boolean updateStore(StoresDTO storesDTO);
    boolean deleteById(int stoId);
}
