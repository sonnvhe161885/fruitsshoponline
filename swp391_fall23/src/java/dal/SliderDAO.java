/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Slider;

/**
 *
 * @author Trung Kien
 */
public class SliderDAO extends DBContext implements GenericDAO<Slider, Integer> {

    @Override
    public boolean create(Slider data) {
        return false;
    }

    @Override
    public boolean update(Slider data) {
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }

    @Override
    public Slider getById(Integer id) {
        return new Slider();
    }

    @Override
    public List<Slider> findAll() {
        String sql = "select * from Sliders where stid = 1";
        List<Slider> sliders = new ArrayList<>();

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Slider slider = new Slider(
                        rs.getInt("slid"),
                        rs.getString("slUrl"),
                        rs.getString("slLink"),
                        rs.getInt("stid")
                );
                sliders.add(slider);
            }
        } catch (Exception ex) {

        }
        return sliders;
    }

}
