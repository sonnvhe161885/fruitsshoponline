/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Customer;
import model.Feedback;
import model.Product;
import model.User;

/**
 *
 * @author sonng
 */
public class FeedbackDAO extends DBContext {

    public ArrayList<Feedback> getListFeedback(String status, String search, String star, User u) {

        if (star.isEmpty()) {
            star = "-1";
        }
        if (status.isEmpty()) {
            status = "-1";
        }
        ArrayList<Feedback> listF = new ArrayList<>();
        String sql = " select * from Feedback f  left join Users u on f.fbCusID =u.uid\n"
                + " left join Products p on f.fbProID = p.pId "
                + "	where ( -1 = " + star + " or  f.fbStar =  " + star + " ) and ( -1 = " + status + " or  f.fbStatus =  " + status + " ) "
                + "  and (u.userName like  LOWER(N'%" + search.toLowerCase() + "%') or p.pName like  LOWER(N'%" + search.toLowerCase() + "%')) ";
        if (u.getRole().getRoleId() == 2) {
            sql += "  and p.[stoId] = " + u.getStore().getStoid();
        }
        System.out.println(sql);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int fbId = rs.getInt(1);
                int fbCusId = rs.getInt(2);
                int fbProId = rs.getInt(3);
                int fbStar = (int) rs.getFloat(4);
                String fbContent = rs.getString(5);
                String fbImage = rs.getString(6);
                Date fbDate = rs.getDate(7);
                int fbStatus = rs.getInt(8);
                Feedback F = new Feedback(fbId, fbCusId, fbProId, fbStar, fbContent, fbImage, fbDate, fbStatus);
                Customer c = new Customer();
                c.setCusName(rs.getString("userName"));
                c.setCusEmail(rs.getString("email"));
                c.setCusPhone(rs.getString("phone"));
                Product p = new Product();
                p.setpName(rs.getString("pName"));
                p.setpBrand(rs.getString("pBrand"));
                F.setCustomer(c);
                F.setProduct(p);
                listF.add(F);
            }
        } catch (Exception e) {
            System.out.println("ListFeedbeck: " + e.getMessage());
            e.printStackTrace();
        }
        return listF;
    }

    public int countTotalFBbyPID(String pid) {
        try {
            String sql = " select count(*) from Feedback f  left join Users u on f.fbCusID =u.uid\n"
                    + " left join Products p on f.fbProID = p.pId "
                    + "	where  f.fbProID = " + pid;
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("ListFeedbeck: " + e.getMessage());
            e.printStackTrace();
        }
        return -1;
    }

    public float avgTotalFBbyPID(String pid) {
        try {
            String sql = "SELECT AVG(f.fbStar) FROM Feedback f "
                    + "LEFT JOIN Users u ON f.fbCusID = u.uid "
                    + "LEFT JOIN Products p ON f.fbProID = p.pId "
                    + "WHERE f.fbProID = ? GROUP BY f.fbProID";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getFloat(1);
            }
        } catch (Exception e) {
            System.out.println("ListFeedback: " + e.getMessage());
            e.printStackTrace();
        }
        return 0f;
    }

    public ArrayList<Feedback> getListFeedbackByPid(String pid, int pagesize, int index) {

        ArrayList<Feedback> listF = new ArrayList<>();
        String sql = " select * from Feedback f  left join Users u on f.fbCusID =u.uid\n"
                + " left join Products p on f.fbProID = p.pId "
                + "	where  f.fbProID = " + pid
                + "  ORDER BY f.fbDate DESC OFFSET " + (index - 1) * pagesize + " ROWS FETCH NEXT 6  ROWS ONLY";
        System.out.println(sql);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int fbId = rs.getInt(1);
                int fbCusId = rs.getInt(2);
                int fbProId = rs.getInt(3);
                int fbStar = (int) rs.getFloat(4);
                String fbContent = rs.getString(5);
                String fbImage = rs.getString(6);
                Date fbDate = rs.getDate(7);
                int fbStatus = rs.getInt(8);
                Feedback F = new Feedback(fbId, fbCusId, fbProId, fbStar, fbContent, fbImage, fbDate, fbStatus);
                Customer c = new Customer();
                c.setCusName(rs.getString("userName"));
                c.setCusEmail(rs.getString("email"));
                c.setCusPhone(rs.getString("phone"));
                Product p = new Product();
                p.setpName(rs.getString("pName"));
                p.setpBrand(rs.getString("pBrand"));
                F.setCustomer(c);
                F.setProduct(p);
                listF.add(F);
            }
        } catch (Exception e) {
            System.out.println("ListFeedbeck: " + e.getMessage());
            e.printStackTrace();
        }
        return listF;
    }

    public static void main(String[] args) {

        System.out.println(new FeedbackDAO().getFeedbackByid("1").getFbImage());
    }

    public Feedback getFeedbackByid(String Fid) {
        String sql = " select * from Feedback f  left join Users u on f.fbCusID =u.uid\n"
                + " left join Products p on f.fbProID = p.pId  where f.[fbID] = " + Fid;
        System.out.println(sql);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int fbId = rs.getInt(1);
                int fbCusId = rs.getInt(2);
                int fbProId = rs.getInt(3);
                int fbStar = (int) rs.getFloat(4);
                String fbContent = rs.getString(5);
                String fbImage = rs.getString(6);
                Date fbDate = rs.getDate(7);
                int fbStatus = rs.getInt(8);
                Feedback F = new Feedback(fbId, fbCusId, fbProId, fbStar, fbContent, fbImage, fbDate, fbStatus);
                Customer c = new Customer();
                c.setCusName(rs.getString("userName"));
                c.setCusEmail(rs.getString("email"));
                c.setCusPhone(rs.getString("phone"));
                Product p = new Product();
                p.setpName(rs.getString("pName"));
                p.setpBrand(rs.getString("pBrand"));
                F.setCustomer(c);
                F.setProduct(p);
                return F;
            }
        } catch (Exception e) {
            System.out.println("ListFeedbeck: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public void insertFeedback(String pid, int uid, String content, String rate, String img) {
        String sql = "INSERT INTO [dbo].[Feedback]\n"
                + "           ([fbCusID]   ,[fbProID] ,[fbStar]\n"
                + "           ,[fbContent] ,[fbImage]\n"
                + "           ,[fbDate],[fbStatus]) VALUES (?,?,?,?,?,GETDATE(),1)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uid);
            st.setString(2, pid);
            st.setString(3, rate);
            st.setString(4, content);
            st.setString(5, img);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("insertFeedback: " + e.getMessage());
            e.printStackTrace();

        }
    }

    public void changeStatusFeedback(String fid, String ss) {
        String sql = "UPDATE [dbo].[Feedback]  SET [fbStatus] = ?\n"
                + " WHERE [fbID] = ? ";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, ss);
            st.setString(2, fid);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("insertFeedback: " + e.getMessage());
            e.printStackTrace();

        }
    }
}
