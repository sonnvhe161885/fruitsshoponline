/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Customer;
import model.Role;
import model.User;

/**
 *
 * @author Admin
 */
public class CustomerDAO extends DBContext{
    
    public List<Customer> findAllCustomer (){
        List<Customer> c = new ArrayList<>();
        try{
            String sql = "SELECT [cusId]\n" +
                            "      ,[cusName]\n" +
                            "      ,[cusEmail]\n" +
                            "      ,[cusPhone]\n" +
                            "      ,[cusAddress]\n" +
                            "      ,[createDate]\n" +
                            "      ,[points]\n" +
                            "  FROM [dbo].[Customer]";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                c.add(new Customer(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),rs.getString(5), rs.getDate(6), rs.getInt(7)));
            }
            return c;
        } catch (Exception e) {
        }
        return null;
    }
    
    public boolean isPhoneExists(String phone) {
        boolean phoneExists = false;
        try{
            String sql = "SELECT COUNT(*) FROM [Customer] WHERE [cusPhone] = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, phone);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                    int count = rs.getInt(1);
                    phoneExists = (count > 0);
            }
        } catch (Exception e) {
            }
        return phoneExists;
    }
    
    public void addcustomer(Customer a) {
        try {

            String sql = "INSERT INTO [dbo].[Customer] ([cusName] , [cusEmail] , [cusPhone] , [cusAddress] ,[createDate], [points])\n"
                    + "VALUES\n"
                    + "		(?, ?, ?, ?, ?, ?)";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getCusName());
            st.setString(2, a.getCusEmail());
            st.setString(3, a.getCusPhone());
            st.setString(4, a.getCusAddress());
            st.setDate(5, a.getCreDate());
            st.setInt(6, a.getPoint());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println();
        }
        
    }
    public static Date getCurrentDate() {
        long currentTimeMillis = System.currentTimeMillis();
        return new Date(currentTimeMillis);
    
    }
    
    public boolean UpdateCustomer(Customer u) {
    try {

        // Cập nhật thông tin người dùng
        String sql = "UPDATE Customer "
                + "SET [cusName] = ?, "
                + "[cusEmail] = ?, "
                + "[cusPhone] = ?, "
                + "[cusAddress] = ?, "
                + "[createDate] = ?,  "
                + "[points] = ?  "
                + "WHERE cusId = ?";
        PreparedStatement st = connection.prepareStatement(sql);
        st.setString(1, u.getCusName());
        st.setString(2, u.getCusEmail());
        st.setString(3, u.getCusPhone());
        st.setString(4, u.getCusAddress());
        st.setDate(5, u.getCreDate());
        st.setInt(6, u.getPoint());
        st.setInt(7, u.getCusId());
        int updated1 = st.executeUpdate();
        if (updated1 > 0) {
            return true;
        }
        // Lấy ID của địa chỉ
        
    } catch (SQLException e) {
        e.printStackTrace();
        
    }
    return false;
    }
    
    public Customer getCustomerById(int id){
        Customer u = null;
        try{
            String sql = "SELECT    [cusId]\n" +
                            "      ,[cusName]\n" +
                            "      ,[cusEmail]\n" +
                            "      ,[cusPhone]\n" +
                            "      ,[cusAddress]\n" +
                            "      ,[createDate]\n" +
                            "      ,[points]\n" +
                            "  FROM [dbo].[Customer] where cusId = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                u = new Customer(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getDate(6), rs.getInt(7));
                
            }
            return u;
        }catch(Exception e){
            
        }
        return null;
        
        
    }
    
    public boolean deleteCus(int id){
        try{
            String sql = " delete customer where cusId = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            int rowsDeleted = st.executeUpdate();
            if(rowsDeleted > 0){
                return true;
            }
            
        }
        catch(Exception e){
            
        }
        return false;
    }
    
//    public static void main(String[] args) {
//        CustomerDAO d = new CustomerDAO();
//        Customer customer = d.getCustomerById(1);
//            System.out.println(customer);
//        
//        
//    }
}
