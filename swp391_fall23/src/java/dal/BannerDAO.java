/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import static dal.UserDAO.convertPassToMD5;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Banner;
import model.User;

/**
 *
 * @author Admin
 */
public class BannerDAO extends DBContext{
    
    public boolean addNewAddress(Banner a){
        try{
            String sql = "INSERT INTO [dbo].[Banner]"
                    + "       ([burl]\n"
                    + "      ,[bdes]\n"
                    + "      ,[stid]\n"
                    + "      ,[blink])\n"
                    + " VALUES\n"
                    + "		(?, ?, ?, ?)";
            PreparedStatement st = connection.prepareStatement(sql);
            
            if(a.getStid() == 1){
                try{
                    String sql1 = "UPDATE banner SET stid = 2 WHERE stid = 1";
                    PreparedStatement st1 = connection.prepareStatement(sql1);
                    st1.executeUpdate();
                }catch(Exception e){
                    
                }
            }
            st.setString(1, a.getBurl());
            st.setString(2, a.getBdes());
            st.setInt(3, a.getStid());
            st.setString(4, a.getBlink());
            st.executeUpdate();
            return true;

        }catch(Exception e){
            
        }
        return false;
    }
    
    public Banner getBannerActive(){
        Banner b = new Banner();
        try{
            String sql = "SELECT [bid]\n"
                    + "      ,[burl]\n"
                    + "      ,[bdes]\n"
                    + "      ,[stid]\n"
                    + "      ,[blink]\n"
                    + "  FROM [dbo].[Banner] where [stid] = 1 ";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                b = new Banner(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5));
            }
        }catch(Exception e){
            
        }
        return b;
    }
    
    public List<Banner> getAllBanner(){
        List<Banner> b = new ArrayList<>();
        try {
            String sql = "SELECT [bid]\n"
                    + "      ,[burl]\n"
                    + "      ,[bdes]\n"
                    + "      ,[stid]\n"
                    + "      ,[blink]\n"
                    + "  FROM [dbo].[Banner]";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Banner banner = new Banner(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5));
                b.add(banner);
            }
        } catch (Exception e) {

        }
        return b;
    }
    
    public boolean UpdateBanner(Banner u) {
    try {

        String sql1 = "UPDATE Banner "
            + "SET [stid] = ? "
            + "WHERE bid = ?";
        PreparedStatement st = connection.prepareStatement(sql1);
        st.setInt(1, u.getStid());
        st.setInt(2, u.getBid());
        int updated1 = st.executeUpdate();
        if (updated1 > 0) {
            String sql2 = "UPDATE banner SET stid = 2 WHERE stid = 1 and bid <> ?";
            PreparedStatement st1 = connection.prepareStatement(sql2);
            st1.setInt(1, u.getBid());
            st1.executeUpdate();
            return true;
        }
        // Lấy ID của địa chỉ
       
    } catch (SQLException e) {
        e.printStackTrace();
        
    }
    return false;
}
    
    public Banner getBannerById(int bid){
        Banner banner = new Banner();
        try {
            String sql = "SELECT [bid]\n"
                    + "      ,[burl]\n"
                    + "      ,[bdes]\n"
                    + "      ,[stid]\n"
                    + "      ,[blink]\n"
                    + "  FROM [dbo].[Banner] where bid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, bid);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                 banner = new Banner(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5));

            }
        } catch (Exception e) {

        }
        return banner;
    }
    
    public boolean deleteBanner(int bid){
        try{
            String sql = "Delete from banner where bid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, bid);
            st.executeUpdate();
            return true;
        }catch(Exception e){
            
        }
        return false;
    }
//    public static void main(String[] args) {
//        BannerDAO b = new BannerDAO();
//        Banner bd = b.getBannerById(2);
//        System.out.println(bd.getBid());
//    }
}
