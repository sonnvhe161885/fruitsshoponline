/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import dal.DBContext;
import dto.BLogDTO;
import dto.BlogCategoryDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author sonng
 */
public class BLogDAO  extends DBContext{

    public ArrayList<BLogDTO> getBlog(String cid, String status, String search, int index, String sort) {
        String sortby = "".equals(sort) ? " order by b.blogId " : sort;
       
        ArrayList<BLogDTO> list = new ArrayList<>();
        String sql = " SELECT * FROM Blog b join CategoryBlog c on b.categoryBlogId = c.categoryBlogId  \n"
                + " join [Status] s on s.stid = b.stid  \n"
                + " where b.blogTitle like '%" + search + "%'  ";
        if (!"".equals(status)) {
            sql += " and b.stid = " + status;
        }
        if (!"".equals(cid)) {
            sql += " and b.categoryBlogId = " + cid;
        }
        sql += sortby
                + "  OFFSET ? ROWS FETCH NEXT 6  ROWS ONLY";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 6);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new BLogDTO(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getDate(4), rs.getInt(5), rs.getInt(6), rs.getString(7), rs.getString(9), rs.getString(12)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public int getNUmberBlog(String cid, String status, String search) {
        String sql = " SELECT count(*) FROM Blog b join CategoryBlog c on b.categoryBlogId = c.categoryBlogId  \n"
                + " join [Status] s on s.stid = b.stid  \n"
                + " where b.blogTitle like '%" + search + "%'  ";
        if (!"".equals(status)) {
            
            sql += " and b.stid = " + status;
        }
        if (!"".equals(cid)) {
            sql += " and b.categoryBlogId = " + cid;
        }
        try {
            
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public BLogDTO getBlogDetail(String blogId) {
        String sql = " SELECT * FROM Blog b join CategoryBlog c on b.categoryBlogId = c.categoryBlogId  \n"
                + " join [Status] s on s.stid = b.stid  \n"
                + " where b.blogId=  " + blogId;
        try {
            
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new BLogDTO(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getDate(4), rs.getInt(5), rs.getInt(6), rs.getString(7), rs.getString(9), rs.getString(12));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<BlogCategoryDTO> getBlogCategoryDTO() {
        ArrayList<BlogCategoryDTO> list = new ArrayList<>();
        String sql = "SELECT * FROM [CategoryBlog]";
        try {
            
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new BlogCategoryDTO(rs.getInt(1), rs.getString(2), rs.getInt(3)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void deleteBlog(int pid) {
        String sql = "  DELETE FROM [Blog] WHERE blogId =?";
        try {
            
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pid);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void updateBlogStatus(int id, int status) {
        String sql = "  UPDATE [Blog]  set [stid]= ?  WHERE blogId =?";
        try {
            
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateBlog(String id, String title, String detail, String cateId, String status, String thumbnail) {
        String sql = "   UPDATE [Blog]  set  [blogTitle] = ?   ,[blogDetail]= ?   ,[categoryBlogId] = ?  ,[stid] = ?  ,[thumbnail]= ?  \n"
                + "    where blogId = ?";
        try {
            
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, title);
            ps.setString(2, detail);
            ps.setString(3, cateId);
            ps.setString(4, status);
            ps.setString(5, thumbnail);
            ps.setString(6, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addBlog(String title, String detail, String cateId, String status, String thumbnail) {
        String sql = "  insert into [Blog] ([blogTitle] ,[blogDetail],[blogDate] ,[categoryBlogId] ,[stid] ,[thumbnail]) \n"
                + "  values (?,?, getdate(),?,?,?)";
        try {
            
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, title);
            ps.setString(2, detail);
            ps.setString(3, cateId);
            ps.setString(4, status);
            ps.setString(5, thumbnail);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
