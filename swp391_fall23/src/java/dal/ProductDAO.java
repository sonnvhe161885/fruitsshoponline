package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;
import model.Image;
import model.Product;
import model.Status;
import model.Store;
import utils.Helpers;

public class ProductDAO extends DBContext {

    private String reason = "ProductDAO -> ";

    public List<Product> getProduct() {
        List<Product> list = new ArrayList<>();

        try {
            String q = "SELECT p.[pId]\n"
                    + "     ,p.[pName]\n"
                    + "     ,p.[pDetail]\n"
                    + "     ,p.[pBrand]\n"
                    + "     ,p.[pSKU]\n"
                    + "	 ,p.[quantity]\n"
                    + "	 ,p.[price]\n"
                    + "	 ,p.[priceIn]\n"
                    + "     ,p.[cid]\n"
                    + "     ,p.[stid] as 'pStatus'\n"
                    + "     ,p.[stoid]\n"
                    + "     ,[cName]\n"
                    + "     ,[cStatus]\n"
                    + "     ,[stoName]\n"
                    + "     ,[stoAddress]\n"
                    + "     ,[stoPhone]\n"
                    + "     ,[stoTimeOpen]\n"
                    + "     ,[stoTimeClosed]\n"
                    + "     ,sto.[stid] as 'stoStatus'\n"
                    + "FROM [dbo].[Products] p\n"
                    + "INNER JOIN [dbo].[Categories] c ON p.cid = c.cid\n"
                    + "INNER JOIN [dbo].[Stores] sto ON p.stoid = sto.stoId";

            PreparedStatement p = connection.prepareStatement(q);

            ResultSet r = p.executeQuery();

            while (r.next()) {
                int pid = r.getInt("pid");

                Status pStatus = getStatus(r.getInt("pStatus"));

                Store sto = new Store(r.getInt("stoid"), r.getString("stoName"), r.getString("stoAddress"), r.getString("stoPhone"),
                        r.getString("stoTimeOpen"), r.getString("stoTimeClosed"), getStatus(r.getInt("stoStatus")));

                Category c = new Category(r.getInt("cid"), r.getString("cName"), r.getBoolean("cStatus"));

                Image i = getImage(pid);

                Product product = new Product(pid, r.getString("pName"), r.getString("pDetail"), r.getString("pBrand"), r.getString("pSKU"),
                        c, pStatus, sto, r.getDouble("price"), r.getDouble("priceIn"), r.getFloat("quantity"), i);

                list.add(product);
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getProduct()", e);
        }

        return list;
    }
    
    public List<Product> getProductByStoreId(int stoId) {
        List<Product> list = new ArrayList<>();

        try {
            String q = "SELECT p.[pId]\n"
                    + "     ,p.[pName]\n"
                    + "     ,p.[pDetail]\n"
                    + "     ,p.[pBrand]\n"
                    + "     ,p.[pSKU]\n"
                    + "	 ,p.[quantity]\n"
                    + "	 ,p.[price]\n"
                    + "	 ,p.[priceIn]\n"
                    + "     ,p.[cid]\n"
                    + "     ,p.[stid] as 'pStatus'\n"
                    + "     ,p.[stoid]\n"
                    + "     ,[cName]\n"
                    + "     ,[cStatus]\n"
                    + "     ,[stoName]\n"
                    + "     ,[stoAddress]\n"
                    + "     ,[stoPhone]\n"
                    + "     ,[stoTimeOpen]\n"
                    + "     ,[stoTimeClosed]\n"
                    + "     ,sto.[stid] as 'stoStatus'\n"
                    + "FROM [dbo].[Products] p\n"
                    + "INNER JOIN [dbo].[Categories] c ON p.cid = c.cid\n"
                    + "INNER JOIN [dbo].[Stores] sto ON p.stoid = sto.stoId "
                    + "WHERE p.stoid = ?";

            PreparedStatement p = connection.prepareStatement(q);
            p.setInt(1, stoId);

            ResultSet r = p.executeQuery();

            while (r.next()) {
                int pid = r.getInt("pid");

                Status pStatus = getStatus(r.getInt("pStatus"));

                Store sto = new Store(r.getInt("stoid"), r.getString("stoName"), r.getString("stoAddress"), r.getString("stoPhone"),
                        r.getString("stoTimeOpen"), r.getString("stoTimeClosed"), getStatus(r.getInt("stoStatus")));

                Category c = new Category(r.getInt("cid"), r.getString("cName"), r.getBoolean("cStatus"));

                Image i = getImage(pid);

                Product product = new Product(pid, r.getString("pName"), r.getString("pDetail"), r.getString("pBrand"), r.getString("pSKU"),
                        c, pStatus, sto, r.getDouble("price"), r.getDouble("priceIn"), r.getFloat("quantity"), i);

                list.add(product);
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getProduct()", e);
        }

        return list;
    }

    public List<Product> getProduct(int category, String price_from, String price_to, String search, int sortBy, int stoid) {
        List<Product> list = new ArrayList<>();

        try {
            String q = "SELECT p.[pId]\n"
                    + "      ,p.[pName]\n"
                    + "      ,p.[pDetail]\n"
                    + "      ,p.[pBrand]\n"
                    + "      ,p.[pSKU]\n"
                    + "      ,p.[cid]\n"
                    + "      ,p.[stid] as 'pStatus'\n"
                    + "      ,p.[stoid] \n"
                    + "	 ,p.[quantity]\n"
                    + "	 ,p.[price]\n"
                    + "	     ,[cName]\n"
                    + "      ,[cStatus]\n"
                    + "	     ,[stoName]\n"
                    + "      ,[stoAddress]\n"
                    + "      ,[stoPhone]\n"
                    + "      ,[stoTimeOpen]\n"
                    + "      ,[stoTimeClosed]\n"
                    + "      ,sto.[stid] as 'stoStatus'\n"
                    + "  FROM [dbo].[Products] p inner join [dbo].[Categories] c\n"
                    + "  on p.cid = c.cid inner join [dbo].Stores sto\n"
                    + "  on p.stoid = sto.stoId\n"
                    + "  WHERE 1=1 and p.[stoid] = ?";

            String subQuery = "SELECT p.[pId]\n"
                    + "     ,p.[pName]\n"
                    + "     ,p.[pDetail]\n"
                    + "     ,p.[pBrand]\n"
                    + "     ,p.[pSKU]\n"
                    + "     ,p.[cid]\n"
                    + "     ,p.[stid] as 'pStatus'\n"
                    + "     ,p.[stoid] \n"
                    + "	 ,p.[quantity]\n"
                    + "	 ,p.[price]\n"
                    + "     ,[cName]\n"
                    + "     ,[cStatus]\n"
                    + "      ,[stoName]\n"
                    + "     ,[stoAddress]\n"
                    + "     ,[stoPhone]\n"
                    + "     ,[stoTimeOpen]\n"
                    + "     ,[stoTimeClosed]\n"
                    + "     ,sto.[stid] as 'stoStatus'\n"
                    + " FROM [dbo].[Products] p inner join [dbo].[Categories] c\n"
                    + " on p.cid = c.cid inner join [dbo].Stores sto\n"
                    + " on p.stoid = sto.stoId\n"
                    + " WHERE 1=1 and p.[stoid] = ? ";

            q = subQuery;

            if (category != 0) {
                q += " AND p.cid = " + category;
            }

            if (price_from != null && price_to != null && !price_from.isEmpty() && !price_to.isEmpty()) {
                List<Double> pid = getListSizeIdByPrice(price_from, price_to);
                q += " AND p.price in (";
                for (int i = 0; i < pid.size(); i++) {
                    if (i == pid.size() - 1) {
                        q += pid.get(i) + ")";
                    } else {
                        q += pid.get(i) + ",";
                    }
                }
            }

            if (search != null && !search.isEmpty()) {
                String[] keywords = search.split(" ");
                String query = "AND (";
                for (int i = 0; i < keywords.length; i++) {
                    if (i > 0) {
                        query += " AND ";
                    }
                    query += " pName like N'%" + keywords[i] + "%'";
                }
                query += ")";
                q += query;
//                q += " AND pName like N'%" + search + "%'";
            }

            if (sortBy != 0) {
                String orderBy = "";
                switch (sortBy) {
                    case 1:
                        orderBy = " ORDER BY pName ASC";
                        break;
                    case 2:
                        orderBy = " ORDER BY pName DESC";
                        break;

                }
                q += orderBy;
            }

            System.out.println("Query: " + q);

            PreparedStatement p = connection.prepareStatement(q);
            p.setInt(1, stoid);

            ResultSet r = p.executeQuery();

            while (r.next()) {

                int pid = r.getInt("pid");

                if (!checkProductExist(list, pid)) {
                    Status pStatus = getStatus(r.getInt("pStatus"));

                    Store sto = new Store(r.getInt("stoid"), r.getString("stoName"), r.getString("stoAddress"), r.getString("stoPhone"),
                            r.getString("stoTimeOpen"), r.getString("stoTimeClosed"), getStatus(r.getInt("stoStatus")));

                    Category c = new Category(r.getInt("cid"), r.getString("cName"), r.getBoolean("cStatus"));

                    Image i = getImage(pid);

                    Product product = new Product(pid, r.getString("pName"), r.getString("pDetail"), r.getString("pBrand"), r.getString("pSKU"),
                            c, pStatus, sto, r.getDouble("price"), r.getFloat("quantity"), i);

                    list.add(product);
                }
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getProduct(N)", e);
        }

        return list;
    }

    public boolean checkProductExist(List<Product> list, int pid) {
        for (Product item : list) {
            if (item.getPid() == pid) {
                return true;
            }
        }
        return false;
    }

    public boolean add(Product p) {
        try {
            String q = "INSERT INTO [dbo].[Products] ([pName] ,[pDetail] ,[pBrand] ,[pSKU] ,[cid] ,[stid] ,[stoid], [quantity], [price],[priceIn], [isOutstanding])\n"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement ps = connection.prepareStatement(q);
            ps.setString(1, p.getpName());
            ps.setString(2, p.getpDetail());
            ps.setString(3, p.getpBrand());
            ps.setString(4, p.getpSKU());
            ps.setInt(5, p.getCategory().getCid());
            ps.setInt(6, p.getStatus().getStid());
            ps.setInt(7, p.getStore().getStoid());
            ps.setFloat(8, p.getQuantity());
            ps.setDouble(9, p.getPrice());
            ps.setDouble(10, p.getPriceIn());
            ps.setBoolean(11, p.isIsOutstanding());
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "add()", e);
        }
        return false;
    }

    public List<Double> getListSizeIdByPrice(String from, String to) {
        List<Double> listPrice = new ArrayList<>();

        try {

            String q = "SELECT price FROM [dbo].[Products]"
                    + "  WHERE price between ? and ?";

            PreparedStatement ps = connection.prepareStatement(q);
            ps.setString(1, from);
            ps.setString(2, to);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                listPrice.add(rs.getDouble(1));
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getListSizeIdByPrice()", e);
        }
        return listPrice;
    }

    public int getLastProductID() {
        try {

            String q = "SELECT TOP(1) pid from Products ORDER BY pid DESC";

            PreparedStatement ps = connection.prepareStatement(q);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getLastProductID()", e);
        }
        return 0;
    }

    public void addImage(Product p) {
        try {

            String q = "INSERT INTO [dbo].[Product_Image] ([iUrl] ,[pid])\n"
                    + "VALUES (? , ?)";

            PreparedStatement ps = connection.prepareStatement(q);
            ps.setString(1, p.getImage().getiUrl());
            ps.setInt(2, p.getPid());

            ps.executeUpdate();

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "addImage()", e);
        }
    }

    public void update(Product p) {
        try {
            String q = "UPDATE [dbo].[Products]\n"
                    + "   SET [pName] = ?\n"
                    + "      ,[pDetail] = ?\n"
                    + "      ,[pBrand] = ?\n"
                    + "      ,[pSKU] = ?\n"
                    + "      ,[quantity] = ?\n"
                    + "      ,[price] = ?\n"
                    + "      ,[cid] = ?\n"
                    + "      ,[stid] = ?\n"
                    + "      ,[priceIn] = ?\n"
                    + " WHERE pid = ?";

            PreparedStatement ps = connection.prepareStatement(q);
            ps.setString(1, p.getpName());
            ps.setString(2, p.getpDetail());
            ps.setString(3, p.getpBrand());
            ps.setString(4, p.getpSKU());
            ps.setFloat(5, p.getQuantity());
            ps.setDouble(6, p.getPrice());
            ps.setInt(7, p.getCategory().getCid());
            ps.setInt(8, p.getStatus().getStid());
            ps.setDouble(9, p.getPriceIn());
            ps.setInt(10, p.getPid());

            updateImage(p);

            ps.executeUpdate();

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "update()", e);
        }
    }

    public Product getProductById(int pid) {

        try {
            String q = "SELECT p.[pId]\n"
                    + "      ,p.[pName]\n"
                    + "      ,p.[pDetail]\n"
                    + "      ,p.[pBrand]\n"
                    + "      ,p.[pSKU]\n"
                    + "      ,p.[cid]\n"
                    + "      ,p.[stid] as 'pStatus'\n"
                    + "      ,p.[stoid] \n"
                    + "	 ,p.[quantity]\n"
                    + "	 ,p.[price]\n"
                    + "	 ,p.[priceIn]\n"
                    + "	     ,pim.[id] as 'iid'\n"
                    + "      ,pim.[iUrl]\n"
                    + "	     ,[cName]\n"
                    + "      ,[cStatus]\n"
                    + "	     ,[stoName]\n"
                    + "      ,[stoAddress]\n"
                    + "      ,[stoPhone]\n"
                    + "      ,[stoTimeOpen]\n"
                    + "      ,[stoTimeClosed]\n"
                    + "      ,sto.[stid] as 'stoStatus'\n"
                    + "  FROM [dbo].[Products] p inner join [dbo].[Product_Image] pim\n"
                    + "  ON p.pId = pim.pid inner join [dbo].[Categories] c\n"
                    + "  on p.cid = c.cid inner join [dbo].Stores sto\n"
                    + "  on p.stoid = sto.stoId\n"
                    + "  WHERE p.pid = ? ";

            PreparedStatement p = connection.prepareStatement(q);
            p.setInt(1, pid);

            ResultSet r = p.executeQuery();

            if (r.next()) {

                Status pStatus = getStatus(r.getInt("pStatus"));

                Store sto = new Store(r.getInt("stoid"), r.getString("stoName"), r.getString("stoAddress"), r.getString("stoPhone"),
                        r.getString("stoTimeOpen"), r.getString("stoTimeClosed"), getStatus(r.getInt("stoStatus")));

                Category c = new Category(r.getInt("cid"), r.getString("cName"), r.getBoolean("cStatus"));

                Image i = getImage(pid);

                Product product = new Product(pid, r.getString("pName"), r.getString("pDetail"), r.getString("pBrand"), r.getString("pSKU"),
                        c, pStatus, sto, r.getDouble("price"),r.getDouble("priceIn"), r.getFloat("quantity"), i);

                return product;
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getProductById()", e);
        }

        return null;
    }

    /**
     *
     * @param stid -> is status id want take from database
     * @return
     */
    public Status getStatus(int stid) {
        try {

            String q = "SELECT * FROM [dbo].[Status] WHERE stid = ?";

            PreparedStatement p = connection.prepareStatement(q);
            p.setInt(1, stid);

            ResultSet r = p.executeQuery();

            if (r.next()) {
                return new Status(r.getInt(1), r.getString(2));
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getStatus()", e);
        }

        return null;
    }

    /**
     *
     * @param pid -> product id want take all size from database
     * @return
     */
    /**
     *
     * @param pid -> product id want take all size from database
     * @return
     */
    public Image getImage(int pid) {

        try {
            String q = "SELECT * FROM .[dbo].[Product_Image] WHERE pid = ?";

            PreparedStatement p = connection.prepareStatement(q);
            p.setInt(1, pid);

            ResultSet r = p.executeQuery();

            while (r.next()) {
                Image i = new Image(r.getInt(1), r.getString(2));
                return i;
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getImage()", e);
        }

        return null;
    }

    public Image updateImage(Product pro) {

        try {
            String q = "UPDATE [dbo].[Product_Image] SET [iUrl] = ? WHERE pid = ?";

            PreparedStatement p = connection.prepareStatement(q);
            p.setString(1, pro.getImage().getiUrl());
            p.setInt(2, pro.getPid());

            p.executeUpdate();

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "updateImage()", e);
        }

        return null;
    }

    public int remove(int pid) {
        System.out.println("pid: " + pid);

        try {
            String sql = ""
                    + "DELETE FROM [dbo].[Product_Image] WHERE pid = ?\n"
                    + "DELETE FROM [dbo].[Products] WHERE pid = ?";
            PreparedStatement ps = connection.prepareStatement(sql);

            ps.setInt(1, pid);
            ps.setInt(2, pid);

            return ps.executeUpdate();

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "remove()", e);
        }

        return -1;

    }

    public List<Product> pagination(List<Product> before, int start, int end) {
        List<Product> after = new ArrayList<>();

        for (int i = start; i < end; i++) {
            after.add(before.get(i));
        }

        return after;
    }
    
    public int countAll() {
        String sql = "select count(*) as total from Products";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("total");
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
  
public int countProductstore(int stoid) {
    String sql = "SELECT COUNT(*) AS total " +
                 "FROM Products p " +
                 "INNER JOIN [dbo].[Categories] c ON p.cid = c.cid " +
                 "INNER JOIN [dbo].[Stores] sto ON p.stoid = sto.stoId " +
                 "WHERE p.stoid = ?";
    try {
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, stoid); // Thiết lập giá trị cho tham số ? trong câu lệnh SQL
        ResultSet rs = ps.executeQuery();
        
        if (rs.next()) {
            return rs.getInt("total");
        }
    } catch (Exception ex) {
        ex.printStackTrace(); // Sử dụng log hợp lý thay vì in ra console
    } finally {
        // Đảm bảo đóng tất cả các tài nguyên, PreparedStatement, ResultSet, Connection, ở đây hoặc trong một phương thức khác
    }
    return 0;
}


   

    public int countAllByTotal(int total) {
        String sql = "select count(*) as total from Products p where p.quantity is null or p.quantity < ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, total);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("total");
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
 public List<Product> getProductOutstanding(int st, int stoid) {
        List<Product> list = new ArrayList<>();

        try {
            String q = "SELECT p.[pId]\n"
                    + "     ,p.[pName]\n"
                    + "     ,p.[pDetail]\n"
                    + "     ,p.[pBrand]\n"
                    + "     ,p.[pSKU]\n"
                    + "	 ,p.[quantity]\n"
                    + "	 ,p.[price]\n"
                    + "	 ,p.[priceIn]\n"
                    + "     ,p.[cid]\n"
                    + "     ,p.[stid] as 'pStatus'\n"
                    + "     ,p.[stoid]\n"
                    + "     ,p.[isOutstanding]\n"
                    + "     ,[cName]\n"
                    + "     ,[cStatus]\n"
                    + "     ,[stoName]\n"
                    + "     ,[stoAddress]\n"
                    + "     ,[stoPhone]\n"
                    + "     ,[stoTimeOpen]\n"
                    + "     ,[stoTimeClosed]\n"
                    + "     ,sto.[stid] as 'stoStatus'\n"
                    + "FROM [dbo].[Products] p\n"
                    + "INNER JOIN [dbo].[Categories] c ON p.cid = c.cid\n"
                    + "INNER JOIN [dbo].[Stores] sto ON p.stoid = sto.stoId where p.[stid] = 1 and p.[isOutstanding] = ? and p.[stoid] = ? ";

            PreparedStatement p = connection.prepareStatement(q);
            p.setInt(1, st);
            p.setInt(2, stoid);
            ResultSet r = p.executeQuery();

            while (r.next()) {
                int pid = r.getInt("pid");

                Status pStatus = getStatus(r.getInt("pStatus"));

                Store sto = new Store(r.getInt("stoid"), r.getString("stoName"), r.getString("stoAddress"), r.getString("stoPhone"),
                        r.getString("stoTimeOpen"), r.getString("stoTimeClosed"), getStatus(r.getInt("stoStatus")));

                Category c = new Category(r.getInt("cid"), r.getString("cName"), r.getBoolean("cStatus"));

                Image i = getImage(pid);

                Product product = new Product(pid, r.getString("pName"), r.getString("pDetail"), r.getString("pBrand"), r.getString("pSKU"),
                        c, pStatus, sto, r.getDouble("price"), r.getDouble("priceIn"), r.getFloat("quantity"), i,r.getBoolean("isOutstanding"));

                list.add(product);
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getProduct()", e);
        }

        return list;
    }
    
    public void UpdateProductOutStanding(int pid, int casee){
        switch(casee){
            case 1:
                try {
                String sql = " update Products set isOutstanding = 0 where pId = ?";
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setInt(1, pid);
                ps.executeUpdate();
            } catch (Exception e) {
            }
                break;
            
            case 2:
                try {
                String sql = " update Products set isOutstanding = 1 where pId = ?";
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setInt(1, pid);
                ps.executeUpdate();
            } catch (Exception e) {
            }
                break;    
        }
    }

   
}
