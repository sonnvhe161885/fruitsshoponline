/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Status;
import utils.Helpers;

public class StatusDAO extends DBContext {

    private String reason = "StatusDAO -> ";

    public List<Status> getAll() {
        List<Status> list = new ArrayList<>();

        try {
            String q = "SELECT * FROM [dbo].[Status] where stType = 'Common'";

            PreparedStatement p = connection.prepareStatement(q);

            ResultSet r = p.executeQuery();

            while (r.next()) {

                list.add(new Status(r.getInt(1), r.getString(2)));
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getAll()", e);
        }

        return list;
    }

    public List<Status> getAllByOrder() {
        List<Status> list = new ArrayList<>();

        try {
            String q = "SELECT * FROM [dbo].[Status] where stType = 'Order'";

            PreparedStatement p = connection.prepareStatement(q);

            ResultSet r = p.executeQuery();

            while (r.next()) {

                list.add(new Status(r.getInt("stid"), r.getString("stName"), r.getString("stNameVi"), r.getString("stType")));
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getAll()", e);
        }

        return list;
    }
    
    public Status getByName(String name) {
        Status status = null;

        try {
            String q = "SELECT * FROM [dbo].[Status] where stName = ?";

            PreparedStatement p = connection.prepareStatement(q);

            p.setString(1, name);
            ResultSet r = p.executeQuery();
            while (r.next()) {

                status = new Status(r.getInt("stid"), r.getString("stName"), r.getString("stNameVi"), r.getString("stType"));
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getAll()", e);
        }

        return status;
    }
}
