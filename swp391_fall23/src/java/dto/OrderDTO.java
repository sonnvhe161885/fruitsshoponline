/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.sql.Connection;
import java.sql.Date;
import model.Address;

/**
 *
 * @author sonng
 */
public class OrderDTO {
    
    int oid;
    double totalPrice;
    String note;
    String phone;
    String name;
    String address;
    String orderCode;
    Date createDate;
    Date deliveryDate;
    int uid;
    int stid;
    String status;
    Address addressModel;

    public OrderDTO(int oid, double totalPrice, String note, String phone, String name, String address, Date createDate, int uid, int stid) {
        this.oid = oid;
        this.totalPrice = totalPrice;
        this.note = note;
        this.phone = phone;
        this.name = name;
        this.address = address;
        this.createDate = createDate;
        this.uid = uid;
        this.stid = stid;
        switch (stid) {
            case 3:
                status = "Chờ xác nhận";
                break;
            case 4:
                status = "Xác nhận";
                break;
            case 5:
                status = "Đang chuẩn bị hàng";
                break;
            case 6:
                status = "Đang giao hàng";
                break;
            case 7:
                status = "Giao hàng thành công"; 
                break;
            case 8:
                status = "Hủy đơn hàng";
                break;
         
        }
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Address getAddressModel() {
        return addressModel;
    }

    public void setAddressModel(Address addressModel) {
        this.addressModel = addressModel;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getStid() {
        return stid;
    }

    public void setStid(int stid) {
        this.stid = stid;
    }

   

}
