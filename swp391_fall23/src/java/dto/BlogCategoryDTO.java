/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author sonng
 */
public class BlogCategoryDTO {

    int categoryBlogId;
    String categoryBlogName;
    int stid;

    public BlogCategoryDTO(int categoryBlogId, String categoryBlogName, int stid) {
        this.categoryBlogId = categoryBlogId;
        this.categoryBlogName = categoryBlogName;
        this.stid = stid;
    }

    public int getCategoryBlogId() {
        return categoryBlogId;
    }

    public void setCategoryBlogId(int categoryBlogId) {
        this.categoryBlogId = categoryBlogId;
    }

    public String getCategoryBlogName() {
        return categoryBlogName;
    }

    public void setCategoryBlogName(String categoryBlogName) {
        this.categoryBlogName = categoryBlogName;
    }

    public int getStid() {
        return stid;
    }

    public void setStid(int stid) {
        this.stid = stid;
    }

}
