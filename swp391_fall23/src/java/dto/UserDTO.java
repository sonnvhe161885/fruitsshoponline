/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author Admin
 */
public class UserDTO {
    private int uid;
    private double totalPrice;
    private int totalOrder;
    private int totalOrderCancel;
    
    public UserDTO() {
    }

    public UserDTO(int uid, double totalPrice, int totalOrder, int totalOrderCancel) {
        this.uid = uid;
        this.totalPrice = totalPrice;
        this.totalOrder = totalOrder;
        this.totalOrderCancel = totalOrderCancel;
    }
    
    public UserDTO(int uid, double totalPrice, int totalOrder) {
        this.uid = uid;
        this.totalPrice = totalPrice;
        this.totalOrder = totalOrder;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getTotalOrder() {
        return totalOrder;
    }

    public void setTotalOrder(int totalOrder) {
        this.totalOrder = totalOrder;
    }

    public int getTotalOrderCancel() {
        return totalOrderCancel;
    }

    public void setTotalOrderCancel(int totalOrderCancel) {
        this.totalOrderCancel = totalOrderCancel;
    }

    
}
