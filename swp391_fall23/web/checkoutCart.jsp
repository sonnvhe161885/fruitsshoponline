<%-- 
    Document   : checkoutCart
    Created on : Oct 10, 2023, 8:46:09 PM
    Author     : Trung Kien
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean class="utils.Helpers" id="helpers"/>
<jsp:useBean class="utils.JsonService" id="jsonService" />
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <style>
            .display {
                display: block !important;
            }
            .blog .carousel-indicators {
                left: 0;
                top: auto;
                bottom: -40px;

            }

            /* The colour of the indicators */
            .blog .carousel-indicators li {
                background: #a3a3a3;
                border: 4px;
                padding: 2px;
            }

            .blog .carousel-indicators .active {
                background: #707070;
            }



            .user-info {
                max-width: 400px;
                margin: 0 0 0 50px;
            }

            .info-item {
                display: flex;
                flex-direction: row;
                align-items: center;
                margin-bottom: 10px;
                margin-top: 20px;
            }

            label {
                flex: 1;
                font-weight: bold;
                margin-right: 10px;
                font-size: 13px;
                font-family: "Quicksand", sans-serif;
            }

            .input-info {
                flex: 2;
                padding: 8px;
                box-sizing: border-box;
                border: 1px solid #ededed;
                border-radius: 5px;
                font-size: 14px;
                font-weight: 800;
                width: 100%; /* Ensure the input and textarea take full width */

                background: #ededed
            }

            textarea {
                height: 100px; /* Set a specific height for the textarea */
            }

            input:focus, textarea:focus {
                border-color: #00a205;
                outline: none;
            }
            h1{
                position: relative;
                text-align: center;
                font-size: 25px;
                margin-top: 30px;
                font-weight: 800;
                line-height: initial;
                font-family: "Quicksand", sans-serif;

            }

            h1::after {
                content: "";
                background: #000;
                display: block;
                width: 60px;
                height: 4px;
                margin: 20px auto;

            }

            h2{
                font-size: 15px;
                margin-top: 50px;
                margin-left: 50px;
                border-bottom: 1px solid #ccc;
                margin-bottom: 20px;
                font-weight: 900;
                padding-bottom: 10px;
                letter-spacing: 1px;
                font-family: "Quicksand", sans-serif;

            }

            h3{
                font-size: 16px;
                font-weight: bold;
                margin-top: 52px;
                letter-spacing: 1px;
                font-family: "Quicksand", sans-serif;
            }

            h4{
                font-size: 14px;
                font-weight: bold;
                margin: 0px;
                font-family: "Quicksand", sans-serif;
            }

            h5{
                text-align: center;
                font-size: 14px;
                font-weight: 900;
                background: #bbffc8;
                padding: 10px;
                margin:0px;
                text-transform: uppercase;
                font-family: "Quicksand", sans-serif;
            }

            .links a {
                color: black; /* Màu chữ ban đầu */
                text-decoration: none; /* Loại bỏ gạch chân mặc định cho liên kết */
                font-size: 13px;
                font-weight: bold;
                font-family: "Quicksand", sans-serif;
            }
            .title-name{
                background-color: #bbffc8;
                border-color: #bbffc8;
                padding: 10px 65px 10px 10px;
                position: relative;

            }

            .address-user{
                background-color: #fbfbfb;
                border-color: #bbffc8;
                padding: 10px 65px 10px 10px;
                position: relative;
            }

            .table{
                margin-top: 50px;
            }

            .clearfix{
                margin-bottom: 15px;
            }

            .input-table{
                background-color: #fbfbfb;
                padding:15px;
                font-family: "Quicksand", sans-serif;
                font-weight: bold;
            }

            #address-heading {
                cursor: pointer;
            }

            .ahai{
                font-family: "Quicksand", sans-serif;
                color: black;
                font-weight: bold;
                font-size: 13px;
            }
            .ahai:hover{
                color: green;
            }
            .links a:hover {
                color: green; /* Màu chữ khi di chuột qua */
            }
            .custom-button {
                font-family: "Quicksand", sans-serif;
                margin-top: 20px;
                font-weight: 900;
                padding: 5px 14px; /* Điều chỉnh padding để nút nhỏ hơn */
                font-size: 14px;
                background-color: white; /* Màu nền trắng */
                color: #00b916; /* Màu chữ xanh */
                border: 2px solid #00b916; /* Viền bo màu xanh */
                border-radius: 5px;
                cursor: pointer;
                margin-bottom: 20px;
            }
            .custom-button:hover {
                background-color: #00b916;
                color: white;
            }
            .custom-button-pass{
                font-family: "Quicksand", sans-serif;
                margin-top: 20px;
                font-weight: 900;
                padding: 5px 14px;
                font-size: 14px;
                background-color: white;
                color: #FF0000;
                border: 2px solid #FF0000;
                border-radius: 5px;
                cursor: pointer;
                margin-bottom: 50px;
            }
            .custom-button-pass:hover {
                background-color: #FF0000;
                color: white;
            }
            .left{
                width: 35%;
            }
            .right{
                width: 65%;
            }

            .large_view{
                display: flex;
            }
            b{
                font-size: 14px;
            }

            p{
                font-size: 13px;
                font-weight: bold;
            }
            .input-textbox{
                margin-bottom: 1px;
                border: 1px solid #000000;
                width: 100%;
                margin-bottom: 15px;
            }

            input::placeholder {
                font-family: "Quicksand", sans-serif;
                font-size: 14px;
                font-weight: bold;
                color:#000000;
            }

            input:focus {
                font-family: "Quicksand", sans-serif; /* Font chữ mong muốn */
                font-size: 15px; /* Kích cỡ font chữ */
                font-weight: bold; /* Độ đậm của font chữ */
                /* ...Thêm các thuộc tính khác theo ý muốn của bạn... */
            }
            .select-address{
                margin-bottom: 15px;
                font-family: "Quicksand", sans-serif;
                font-weight: 600;
                font-size: 17px;
            }
            select option{
                font-family: "Quicksand", sans-serif;
                font-weight: 600;
            }
            .v3_select{
                pointer-events: none;
            }
        </style>
    </head>

    <body>
        <c:if test="${not empty message}">
            <c:if test="${alert.equals('success')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-success alert-dismissible fade show text-center position-absolute " role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${alert.equals('danger')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-danger alert-dismissible fade show text-center position-absolute " role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${alert.equals('Warning')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-warning alert-dismissible fade show text-center position-absolute " role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
        </c:if>
        <jsp:include page="header.jsp" flush="true" >
            <jsp:param name="shop" value="active" />
        </jsp:include>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->

        <!-- Hero Section End -->

        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-section set-bg" style="background-image: url('img/breadcrumb.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="breadcrumb__text">
                            <h2>Giỏ hàng</h2>
                            <div class="breadcrumb__option">
                                <a href="./home">Trang chủ</a>
                                <span>Giỏ hàng</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Checkout Section Begin -->
        <section class="checkout spad">
            <div class="container">
                <div class="checkout__form">
                    <h4>Thông tin đơn hàng của bạn</h4>
                    <form action="${pageContext.request.contextPath}/cartCompletion" method="POST" style="color: #000">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="checkout__input">
                                    <h4>Cửa hàng: ${store.stoName} - ${store.stoAddress}</h4>
                                    <input name="storeId" value="${store.stoId}" type="hidden"/>
                                </div>
                                <div class="checkout__input">
                                    <p>Danh sách địa chỉ<span>*</span></p>
                                    <select id="address-select" class="form-control select2" name="addressId">
                                        <c:if test="${addressDefault != null}">
                                            <option value="${addressDefault.getAddressId()}" class="d-block flex-column">
                                            <h5 class="font-weight-bold">
                                                ${addressDefault.getAddress()}, ${addressDefault.getCommune()}, ${addressDefault.getDistrict()}, ${addressDefault.getCity()}
                                            </h5>
                                            </option>
                                        </c:if>
                                        <c:if test="${addressDefault == null}">
                                            <option selected value="">Chọn địa chỉ</option>
                                        </c:if>
                                        <c:forEach items="${addresses}" var="address">
                                            <option value="${address.getAddressId()}" class="d-block flex-column">
                                            <h5 class="font-weight-bold">
                                                ${address.getAddress()}, ${address.getCommune()}, ${address.getDistrict()}, ${address.getCity()}
                                            </h5>
                                            </option>
                                        </c:forEach>
                                        <!-- Thêm các option khác nếu cần -->
                                    </select>
                                </div>
                                <div class="checkout__input">
                                    <p>Tên đầy đủ<span>*</span></p>
                                    <input style="color: #000"  pattern="^(?!\s*$).+" title="Tên không đúng định dạng" required="true" type="text" placeholder="Full name" name="fullname" value="${sessionScope.account.userName}">
                                </div>
                                <div class="checkout__input">
                                    <p>Email<span>*</span></p>
                                    <input style="color: #000" required="true" type="email" placeholder="email" name="email" value="${sessionScope.account.email}">
                                </div>
                                <div class="checkout__input">
                                    <p>Số điện thoại<span>*</span></p>
                                    <input style="color: #000" required="true" type="text" pattern="0[0-9]{9}" title="Số điện thoại bắt đầu bằng 0 và có 10 số" placeholder="Phone" class="checkout__input__add" name="phone" value="${sessionScope.account.phone}">
                                </div>
                                <div class="checkout__input">
                                    <p>Ðịa chỉ<span>*</span></p>
                                    <c:if test="${addressDefault != null}" >
                                        <input id ="address-input" style="color: #000"  pattern="^(?!\s*$).+" title="Địa chỉ không đúng định dạng"  required="true" type="text" placeholder="Address" name="address"  value="${addressDefault.getAddress()}, ${addressDefault.getCommune()}, ${addressDefault.getDistrict()}, ${addressDefault.getCity()}">
                                    </c:if>
                                    <c:if test="${addressDefault == null}" >
                                        <input id ="address-input" style="color: #000"  pattern="^(?!\s*$).+" title="Địa chỉ không đúng định dạng"  required="true" type="text" placeholder="Address" name="address"  >
                                    </c:if>
                                </div>
                                <div class="checkout__input d-flex justify-content-between" >
                                    <div class="input-address">
                                        <select id="city" class="select-address form-control" name ="city">
                                            <option value="" selected>Chọn tỉnh thành</option>           
                                        </select>
                                        <input type="hidden" name="cityText" id="cityText">
                                    </div>
                                    <div class="input-address">
                                        <select id="district" class="select-address form-control" name="district">
                                            <option value="" selected>Chọn quận huyện</option>
                                        </select>
                                        <input type="hidden" name="districtText" id="districtText">
                                    </div>
                                    <div class="input-address" >
                                        <select id="ward"  class="select-address form-control" name="ward">
                                            <option value="" selected>Chọn phường xã</option>
                                        </select>
                                        <input type="hidden" name="wardText" id="wardText">
                                    </div>    
                                </div>


                                <div class="checkout__input">
                                    <p>Ghi chú</p>
                                    <input type="note" placeholder="Note" name="note">
                                </div>
                                <div>
                                    <input type="checkbox"  checked="true"/>
                                    <span>Thanh toán khi nhận hàng</span>
                                </div>

                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="checkout__order">
                                    <h4>Đơn hàng của bạn</h4>
                                    <table class="w-100">
                                        <thead>
                                            <tr>
                                                <th style="width: 40%">Sản phẩm</th>                                                
                                                <th style="width: 20%">Giá tiền/Kg</th>
                                                <th style="width: 10%">Số lượng</th>
                                                <th style="width: 20%">Tổng tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="product" items="${productDisplayCartDTOList}">
                                                <tr style="border-bottom: 1px solid #e1e1e1; border-top: 1px solid #e1e1e1;">
                                                    <td>
                                                        <div style="margin: 8px 0">
                                                            <img src="img/product/${product.getImageUrl()}" style="height: 40px; width: 40px" alt="">
                                                            ${product.getPName()}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div style="margin: 8px 0">
                                                            ${helpers.formatInt(product.price)} VNĐ
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div style="margin: 8px 0">
                                                            ${product.getQuantity()} kg
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div style="margin: 8px 0">
                                                            ${helpers.formatInt(product.price * product.getQuantity())} VNĐ
                                                        </div>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <!--<div class="checkout__order__products">Products <span>Price </span> <span>Quantity</span></div>-->
                                    <!--                                    <ul>
                                    <c:forEach var="product" items="${productDisplayCartDTOList}">
                                        <li>
                                            <img src="${product.getImageUrl()}" style="height: 40px; width: 40px" alt="">${product.getPName()} 
                                            <span></span>
                                            <span>${product.getQuantity()} kg</span>
                                        </li>
                                    </c:forEach>

                                    <input type="hidden" name="productsOrder" value="${jsonService.stringToBase64(jsonService.toString(productDisplayCartDTOList))}"/>
                                </ul>-->
                                    <input type="hidden" name="productsOrder" value="${jsonService.stringToBase64(jsonService.toString(productDisplayCartDTOList))}"/>
                                    <div class="checkout__order__subtotal">Phí vẫn chuyển<span>Không mất phí</span></div>
                                    <div class="checkout__order__total">Tổng thanh toán <span>${helpers.formatInt(requestScope.total)} VNÐ</span></div>
                                    <button type="submit" class="site-btn">Ðặt hàng</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- Checkout Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="home"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul>
                                <li>Của hàng HCM: 66 Nguyễn Trí Sách, Phường 15 Tân Bình</li>
                                <li>Cửa hàng HN: 63 Yên Lãng - Đống Đa - Hà Nội</li>
                                <li>Phone: 0123456789</li>
                                <li>Email: shophoaqua@gmail.com</li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
        <script src="js/app.js"></script>

        <script>
                                        var department = document.querySelector(".hero__categories__all");
                                        department.addEventListener('click', function () {
                                            var ulTag = document.getElementById('categories_departments');
                                            console.log(ulTag);
                                            let classList = ulTag.classList;
                                            if (classList.contains('display')) {
                                                classList.remove('display');
                                            } else {
                                                classList.add('display');
                                            }
                                        })
        </script>
        <script>
            // Sử dụng JavaScript hoặc jQuery để tự động đóng alert sau một khoảng thời gian
            setTimeout(function () {
                $("#myAlert").alert('close'); // Sử dụng jQuery
                // Hoặc sử dụng JavaScript thuần
                // document.getElementById('myAlert').style.display = 'none';
            }, 3000); // 3000 milliseconds tương ứng với 3 giây
        </script>
        <script>
            var select = document.getElementById("address-select");

            select.addEventListener("change", function (e) {
                console.log(e);
                if (e.target.value !== "") {
                    document.getElementById("address-input").value = $("#address-select option:selected").text().trim();
                    $('#city').attr('disabled', 'disabled');
                    $('#district').attr('disabled', 'disabled');
                    $('#ward').attr('disabled', 'disabled');
                } else {
                    document.getElementById("address-input").value = "";
                    $('#city').attr('disabled', false);
                    $('#district').attr('disabled', false);
                    $('#ward').attr('disabled', false);
                }
            });
        </script>

        <script>

            function confirmDelete(addressId) {
                if (confirm("Bạn có chắc chắn muốn xóa?")) {
                    // Nếu người dùng đồng ý xóa, gọi servlet để xóa thông tin
                    window.location.href = 'deleteaddress?aid=' + addressId;
                }
            }


            // Lấy tham chiếu đến thẻ select
            var select = document.getElementById("city");
            var select1 = document.getElementById("district");
            var select2 = document.getElementById("ward");
            // Lấy tham chiếu đến trường ẩn
            var hiddenField = document.getElementById("cityText");
            var hiddenField1 = document.getElementById("districtText");
            var hiddenField2 = document.getElementById("wardText");

            // Thêm sự kiện onchange để cập nhật trường ẩn khi người dùng thay đổi tùy chọn
            select.addEventListener("change", function () {
                hiddenField.value = select.options[select.selectedIndex].text;
            });
            select1.addEventListener("change", function () {
                hiddenField1.value = select1.options[select1.selectedIndex].text;
            });
            select2.addEventListener("change", function () {
                hiddenField2.value = select2.options[select2.selectedIndex].text;
            });
        </script>
    </body>

</html>