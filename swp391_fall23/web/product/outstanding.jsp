<%-- 
    Document   : UserManage
    Created on : Sep 18, 2023, 8:44:34 PM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<jsp:useBean class="utils.Helpers" id="helpers"/>
<!DOCTYPE html>
<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Danh sách Khách Hàng | Quản trị Admin</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="home"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../navbar.jsp" flush="true" >
            <jsp:param name="product" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="product-listing"><b>Danh sách sản phẩm</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h5>Danh sách sản phẩm nổi bật</h5>

                             <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50">Id</th>
                                        <th width="150">Ảnh sản phẩm</th>
                                        <th width="250">Tên sản phẩm</th>
                                        <th>Chức năng</th>
                                    </tr>
                                    <c:forEach items="${list1}" var="l1">
                                <input type="hidden" name ="pid" value="${l1.pid}">
                                <input type="hidden" name ="case" value="1">
                                <tbody>
                                    <td>${l1.pid}</td>
                                    <td><img src="img/product/${l1.image.iUrl}" alt="" height="50px" width="50px"></td>
                                    <td>${l1.pName}</td>
                                    <td>
                                        <form action="outstanding" method="post">
                                        <input type="hidden" name ="pid" value="${l1.pid}">
                                        <input type="hidden" name ="case" value="1">
                                        <input class ="button-date" type="submit" value="Gỡ">
                                        </form>
                                    </td>
                                </tbody>
                                </c:forEach>
                            </table>    

                        </div>
                    </div>
                    </div>
                </div>
                
                <div class="col-lg-6">
                    <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                           
                            <h5>Danh sách sản phẩm</h5>
                             <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50">Id</th>
                                        <th width="150">Ảnh sản phẩm</th>
                                        <th width="250">Tên sản phẩm</th>
                                        <th>Chức năng</th>
                                    </tr>
                                <c:forEach items="${list2}" var="l2">
                               
                                <tbody>
                                    <td>${l2.pid}</td>
                                    <td><img src="img/product/${l2.image.iUrl}" alt="" height="50px" width="50px"></td>
                                    <td>${l2.pName}</td>
                                    <td>
                                         <form action="outstanding" method="post">
                                              <input type="hidden" name ="pid" value="${l2.pid}">
                                <input type="hidden" name ="case" value="2">
                                        <input class ="button-date" type="submit" value="Thêm">
                                        </form>
                                    </td>
                                </tbody>
                                </c:forEach>
                            </table>
                            
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            
        </main>

        <style>
            select{
                margin-right:10px;
                border: 2px solid black; /* Viền đen 2px */
                border-radius: 10px;
                size: 20px;
            }
            .button-date{
                border: none; /* Bỏ viền */
                border-radius: 10px; /* Làm tròn nút với bán kính 10px */
                background-color: #FBE2C5; /* Màu nền của nút */
                color: #F59D39; /* Màu văn bản */
                margin: auto 0;
                cursor: pointer;
                width:100px;
                font-weight: bold;
            }
        </style>

        <!-- Essential javascripts for application to work-->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="src/jquery.table2excel.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <!-- Data table plugin-->
        <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">$('#sampleTable').DataTable();</script>
        <script>

            function confirmDelete(button) {
                // Lấy form cha của button được nhấn
                var form = button.closest('.deleteForm');

                // Lấy giá trị userId từ form
                var cusId = form.querySelector('.cusId').value;

                if (confirm('Bạn có chắc chắn muốn xóa người dùng có id: ' + cusId + ' không?')) {
                    // Gán giá trị userId vào form và submit
                    form.submit();
                }
            }


            //Thời Gian
            function time() {
                var today = new Date();
                var weekday = new Array(7);
                weekday[0] = "Chủ Nhật";
                weekday[1] = "Thứ Hai";
                weekday[2] = "Thứ Ba";
                weekday[3] = "Thứ Tư";
                weekday[4] = "Thứ Năm";
                weekday[5] = "Thứ Sáu";
                weekday[6] = "Thứ Bảy";
                var day = weekday[today.getDay()];
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                nowTime = h + " giờ " + m + " phút " + s + " giây";
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                        '</span>';
                document.getElementById("clock").innerHTML = tmp;
                clocktime = setTimeout("time()", "1000", "Javascript");

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
            }
            //In dữ liệu
            var myApp = new function () {
                this.printTable = function () {
                    var tab = document.getElementById('sampleTable');
                    var win = window.open('', '', 'height=700,width=700');
                    win.document.write(tab.outerHTML);
                    win.document.close();
                    win.print();
                }
            }

        </script>
    </body>

</html>
