

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<jsp:useBean class="dal.CategoryDAO" id="cdao" />
<jsp:useBean class="dal.StatusDAO" id="stdao" />
<jsp:useBean class="dal.ProductDAO" id="pdao" />
<jsp:useBean class="utils.Helpers" id="helpers"/>

<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Danh sách Sản Phẩm | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="home"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../navbar.jsp" flush="true">
            <jsp:param name="product" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Danh sách sản phẩm</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">
                            <div class="row element-button">
                                <div class="col-sm-2">

                                    <a class="btn btn-add btn-sm" href="product-add" title="Thêm"><i
                                            class="fas fa-plus"></i>
                                        Tạo mới sản phẩm</a>
                                </div>
                                <div class="col-sm-2">

                                    <a class="btn btn-add btn-sm" href="outstanding" title="Danh sách sản phẩm nổi bật"><i
                                            class="fas fa-plus"></i>
                                        Sản phẩm nổi bật</a>
                                </div>
                            </div>
                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                    <tr>

                                        <th>ID</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Giá nhập</th>
                                        <th>Giá bán</th>
                                        <th>Số lượng</th>
                                        <th>Cửa hàng</th>
                                        <th>Danh mục sản phẩm</th>
                                        <th>Trạng thái sản phẩm</th>
                                        <th>Chức năng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${data}" var="item">
                                        <tr> 

                                            <td>${item.pid} </td>
                                            <td>${item.pName}</td>
                                            <td>${helpers.formatInt(item.priceIn)} VNĐ</td>
                                            <td>${helpers.formatInt(item.price)} VNĐ</td>
                                            <td>${item.quantity} kg</td>
                                            <td>${item.store.stoName}</td>
                                            <td>${item.category.cName}</td>
                                            <td><span
                                                    class="badge ${item.status.stid == 1 ? 'bg-success' : item.status.stid == 2 ? 'bg-danger' : 'bg-warn'}">${item.status.stName}</span>
                                            </td>
                                            <td>
                                                <form action="${pageContext.request.contextPath}/product-remove"
                                                      onsubmit="return myFunction(this)"
                                                      id="form-remove_${item.pid}">
                                                    <button class="btn btn-primary btn-sm trash" onclick="return confirm('Are you sure to delete Product !')"
                                                            value="${item.pid}" type="submit" title="Xóa">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                    <input type="hidden" name="id" value="${item.pid}">


                                                    <button class="btn btn-primary btn-sm edit" type="button"
                                                            title="Sửa" id="show-emp" data-toggle="modal"
                                                            data-target="#edit_product_${item.pid}">
                                                        <i class="fas fa-edit"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    <div class="modal fade" id="edit_product_${item.pid}" tabindex="-1"
                                         role="dialog" aria-hidden="true" data-backdrop="static"
                                         data-keyboard="false">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <form action="product-edit" method="POST">
                                                        <input type="text" name="action" value="${url}"
                                                               hidden>
                                                        <div class="row">
                                                            <div class="form-group  col-md-12">
                                                                <span class="thong-tin-thanh-toan">
                                                                    <h5>Chỉnh sửa thông tin sản phẩm cơ bản
                                                                    </h5>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">ID
                                                                </label>
                                                                <input class="form-control" type="text"
                                                                       name="pid" readonly value="${item.pid}">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">Tên sản
                                                                    phẩm</label>
                                                                <input class="form-control" type="text"
                                                                       name="pName" required
                                                                       value="${item.pName}">
                                                            </div>
                                                            <div class="form-group col-md-6 ">
                                                                <label for="exampleSelect1"
                                                                       class="control-label">Danh mục sản phẩm</label>
                                                                <select class="form-control"
                                                                        id="exampleSelect1" name="cid">
                                                                    <c:forEach items="${cdao.all}" var="c">
                                                                        <option value="${c.cid}"
                                                                                ${c.cid==item.category.cid
                                                                                  ? 'selected' : '' }>${c.cName}
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-6 ">
                                                                <label for="exampleSelect1"
                                                                       class="control-label">Trạng thái sản phẩm</label>
                                                                <select class="form-control"
                                                                        id="exampleSelect1" name="stid">
                                                                    <c:forEach items="${stdao.all}"
                                                                               var="st">
                                                                        <option value="${st.stid}"
                                                                                ${st.stid==item.status.stid
                                                                                  ? 'selected' : '' }>${st.stName}
                                                                        </option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <BR>
                                                        <a href="${pageContext.request.contextPath}/product-edit?pid=${item.pid}"
                                                           style="    float: right;
                                                           font-weight: 600;
                                                           color: #ea0000;">Chỉnh sửa sản phẩm nâng cao</a>
                                                        <BR>
                                                        <BR>
                                                        <button class="btn btn-save" type="submit">Lưu
                                                            lại</button>
                                                        <a class="btn btn-cancel" data-dismiss="modal"
                                                           href="#">Hủy bỏ</a>
                                                        <BR>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <!--
MODAL
        -->

        <!-- Essential javascripts for application to work-->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="src/jquery.table2excel.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <!-- Data table plugin-->
        <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">
                                                        $('#sampleTable').DataTable();
                                                        //Thời Gian
                                                        function time() {
                                                            var today = new Date();
                                                            var weekday = new Array(7);
                                                            weekday[0] = "Chủ Nhật";
                                                            weekday[1] = "Thứ Hai";
                                                            weekday[2] = "Thứ Ba";
                                                            weekday[3] = "Thứ Tư";
                                                            weekday[4] = "Thứ Năm";
                                                            weekday[5] = "Thứ Sáu";
                                                            weekday[6] = "Thứ Bảy";
                                                            var day = weekday[today.getDay()];
                                                            var dd = today.getDate();
                                                            var mm = today.getMonth() + 1;
                                                            var yyyy = today.getFullYear();
                                                            var h = today.getHours();
                                                            var m = today.getMinutes();
                                                            var s = today.getSeconds();
                                                            m = checkTime(m);
                                                            s = checkTime(s);
                                                            nowTime = h + " giờ " + m + " phút " + s + " giây";
                                                            if (dd < 10) {
                                                                dd = '0' + dd
                                                            }
                                                            if (mm < 10) {
                                                                mm = '0' + mm
                                                            }
                                                            today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                                                            tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                                                                    '</span>';
                                                            document.getElementById("clock").innerHTML = tmp;
                                                            clocktime = setTimeout("time()", "1000", "Javascript");

                                                            function checkTime(i) {
                                                                if (i < 10) {
                                                                    i = "0" + i;
                                                                }
                                                                return i;
                                                            }
                                                        }
        </script>

        <script>
            function myFunction(e) {
                ;
            }
        </script>
    </body>
</html>