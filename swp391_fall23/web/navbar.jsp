<%-- 
    Document   : NavBar
    Created on : Sep 19, 2023, 9:40:53 PM
    Author     : Trung Kien
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="img/icon.png" width="50px"
                                        alt="User Image">
        <div>
            <p class="app-sidebar__user-name"><b>${sessionScope.account.userName}</b></p>
            <p class="app-sidebar__user-designation">Chào mừng bạn trở lại</p>
        </div>
    </div>
    <hr>
    <ul class="app-menu">
        <c:if test="${sessionScope.account.role.roleName != 'Admin'}">
        <li><a id ="home" onclick="homeClick()" class="app-menu__item ${param.home}" href="<c:url value="/Sellerdashboard" />"><i class='app-menu__icon bx bx-tachometer'></i><span
                    class="app-menu__label">Trang chủ</span></a></li>
                    </c:if>
        <!--Quản lí store-->
        <c:if test="${sessionScope.account.role.roleName == 'Admin'}">
            <li><a id ="home" onclick="homeClick()" class="app-menu__item ${param.home}" href="<c:url value="/dashboard" />"><i class='app-menu__icon bx bx-tachometer'></i><span
                    class="app-menu__label">Trang chủ</span></a></li>
            <li>
                <a id="store" onclick="storeClick()" class="app-menu__item ${param.store}" href="<c:url value="/manager-stores" />">
                    <i class='app-menu__icon bx bx-store'></i>
                    <span class="app-menu__label">Quản lý cửa hàng</span>
                </a>
            </li>
        </c:if>
        <c:if test="${sessionScope.account.role.roleName != 'Admin'}">
        <li><a id="product" onclick="productClick()" class="app-menu__item ${param.product}" href="<c:url value="/product-listing" />"><i class='app-menu__icon bx bx-purchase-tag-alt'>
                </i><span class="app-menu__label">Quản lý sản phẩm</span></a>
        </c:if>        
        <li><a id="store" onclick="storeClick()" class="app-menu__item ${param.orders}" href="<c:url value="/manager-orders" />"><i class='app-menu__icon bx bxs-shopping-bags'></i> <span
                    class="app-menu__label">Quản lý đơn hàng</span></a></li>
        <c:if test="${sessionScope.account.role.roleName == 'Admin'}">            
        <li><a id="post" onclick="postClick()" class="app-menu__item ${param.post}" href="<c:url value="./BlogList" />"><i class='app-menu__icon bx bx-user-voice'></i>
                <span class="app-menu__label">Quản lý bài đăng</span></a></li>
        </c:if>        
        <li><a id="post" onclick="postClick()" class="app-menu__item ${param.fedback}" href="<c:url value="./FeedbackList" />"><i class='app-menu__icon bx bx-user-voice'></i>
                <span class="app-menu__label">Quản lý Feedback</span></a></li>

        <c:if test="${sessionScope.account.role.roleName == 'Admin'}">
        <li><a id="seller" onclick="sellerClick()" class="app-menu__item ${param.seller}" href="<c:url value="/seller" />"><i class='app-menu__icon bx bx-id-card'></i>
                <span class="app-menu__label">Quản lý người dùng</span></a></li>

        
        

        <li><a id="post" onclick="postClick()" class="app-menu__item ${param.banner}" href="<c:url value="/banner" />"><i class='app-menu__icon bx bx-calendar-check'></i>
                <span class="app-menu__label">Quản Lý Banner </span></a></li>
        </c:if>
        
    </ul>

</aside>

