/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
const thumbnails = document.querySelectorAll(".thumbnail");
const modal = document.getElementById("imageModal");
const modalImage = document.getElementById("modalImage");
const closeBtn = document.querySelector(".close");

function openModal(src) {
    modal.style.display = "block";
    modalImage.src = src;
    document.body.classList.add("blur");
}

function closeModal() {
    modal.style.display = "none";
    document.body.classList.remove("blur");
}

thumbnails.forEach(thumbnail => {
    thumbnail.addEventListener("click", () => {
        const src = thumbnail.getAttribute("data-src");
        openModal(src);
    });
});

closeBtn.addEventListener("click", closeModal);

window.addEventListener("click", (event) => {
    if (event.target == modal) {
        closeModal();
    }
});

