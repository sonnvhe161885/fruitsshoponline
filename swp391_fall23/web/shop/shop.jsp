<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean class="dal.CategoryDAO" id="cdao"/>
<jsp:useBean class="utils.Helpers" id="helpers"/>
<jsp:useBean class="dal.SizeDAO" id="sdao" />
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">

        <style>
            .product__pagination label {
                display: inline-block;
                width: 30px;
                height: 30px;
                border: 1px solid #b2b2b2;
                font-size: 14px;
                color: #b2b2b2;
                font-weight: 700;
                line-height: 28px;
                text-align: center;
                margin-right: 16px;
                -webkit-transition: all, 0.3s;
                -moz-transition: all, 0.3s;
                -ms-transition: all, 0.3s;
                -o-transition: all, 0.3s;
                transition: all, 0.3s;
            }

            .product__pagination label:hover{
                background: #7fad39;
                border-color: #7fad39;
                color: #ffffff;
                cursor: pointer;
            }

            .product__pagination label.active {
                background-color: #7fad39;
            }

            .sidebar__item ul li.active {
                font-weight: bold
            }

            .sidebar__item ul li label{
                font-size: 16px;
                color: #1c1c1c;
                line-height: 39px;
                display: block;
            }

            .sidebar__item ul li label:hover{
                cursor: pointer;
                color: #7fad39;
            }

            .sidebar__item__size label.active {
                background: #7fad39;
                color: white;
                border: 1px solid red;
            }

            .sidebar__item__box.scroll {
                max-height: 27vh;
                overflow: auto
            }
        </style>
    </head>

    <body>


        <!-- Header Section End -->
        <jsp:include page="../header.jsp" flush="true" >
            <jsp:param name="shop" value="active" />
        </jsp:include>
        <c:if test="${not empty param.message}">
            <c:if test="${param.alert.equals('success')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-success alert-dismissible fade show text-center position-absolute " role="alert">
                    ${param.message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${param.alert.equals('danger')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-danger alert-dismissible fade show text-center position-absolute " role="alert">
                    ${param.message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${param.alert.equals('Warning')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-warning alert-dismissible fade show text-center position-absolute " role="alert">
                    ${param.message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
        </c:if>



        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <!--                        <div class="hero__categories">
                                                    <div class="hero__categories__all">
                                                        <i class="fa fa-bars"></i>
                                                        <span>All departments</span>
                                                    </div>
                                                    <ul>
                                                        <li><a href="#">Fresh Meat</a></li>
                                                        <li><a href="#">Vegetables</a></li>
                                                        <li><a href="#">Fruit & Nut Gifts</a></li>
                                                        <li><a href="#">Fresh Berries</a></li>
                                                        <li><a href="#">Ocean Foods</a></li>
                                                        <li><a href="#">Butter & Eggs</a></li>
                                                        <li><a href="#">Fastfood</a></li>
                                                        <li><a href="#">Fresh Onion</a></li>
                                                        <li><a href="#">Papayaya & Crisps</a></li>
                                                        <li><a href="#">Oatmeal</a></li>
                                                        <li><a href="#">Fresh Bananas</a></li>
                                                    </ul>
                                                </div>-->
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="${pageContext.request.contextPath}/shop" method='post'>

                                    <input type="text" name='search' value='${search}' placeholder="Bạn đang tìm kiếm gì ?">
                                    <button type="submit" class="site-btn">Search</button>
                                </form>
                            </div>
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>0123456789</h5>
                                    <span>Hỗ trợ từ 7:00 - 18:00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Breadcrumb Section Begin -->

        <!-- Breadcrumb Section End -->

        <!-- Product Section Begin -->
        <section class="product spad">
            <div class="container">
                <form action="" method="POST" id='form_shop'>
                    <div class="row">
                        <div class="col-lg-3 col-md-5">
                            <div class="sidebar">
                                <div class="sidebar__item">
                                    <h4>Danh mục sản phẩm</h4>
                                    <ul>
                                        <c:forEach items="${cdao.all}" var="item">
                                            <li class="${cid == item.cid ? 'active' : ''} ">
                                                <input hidden type='radio' name='cid' value="${item.cid}"
                                                       ${cid != null && cid == item.cid ? 'checked' : '' } onchange="this.form.submit()" id='category_${item.cid}' />
                                                <label for='category_${item.cid}'>${item.cName}</label>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="sidebar__item">
                                    <h4>Giá tiền</h4>
                                    <div class="price-range-wrap">
                                        <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                             data-min="0" data-max="1000000">
                                            <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                                            <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                            <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                        </div>
                                        <div class="range-slider">
                                            <div class="price-input">
                                                <input type="text" name='price_from' id="minamount" onchange="this.form.submit()" value="${price_from}">
                                                <input type="text" name='price_to'id="maxamount" value="${price_to}">
                                            </div>
                                        </div>
                                    </div>
                                    <button id='filter-price' type='button'>Lọc giá</button>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-9 col-md-7">

                            <div class="filter__item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-5">
                                        <div class="filter__sort">
                                            <span>Sắp xếp</span>
                                            <select name="sortBy" onchange="this.form.submit()">
                                                <option value="">Choose</option>
                                                <option value="1" ${sortBy != null && sortBy == 1 ? 'selected' :''}>Tên: A-Z</option>
                                                <option value="2" ${sortBy != null && sortBy == 2 ? 'selected' :''}>Tên: Z-A</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="filter__found">
                                            <h6><span>${size}</span> Sản phẩm</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-3">
                                        <div class="filter__option">
                                            <span class="icon_grid-2x2"></span>
                                            <span class="icon_ul"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <c:if test="${data.isEmpty()}">
                                <div class="row">
                                    <h3 class="text-danger text-center w-100 mt-5">Không tìm thấy sản phẩm nào!</h3>
                                </div>
                            </c:if>
                            <c:if test="${!data.isEmpty()}">
                                <div class="row">
                                    <c:forEach items="${data}" var="item">
                                        <c:if test="${item.status.stid == 1}">
                                        <div class="col-lg-4 col-md-6 col-sm-6">
                                            <div class="product__item">
                                                <div class="product__item__pic set-bg" data-setbg="img/product/${item.image.iUrl}">
                                                    <ul class="product__item__pic__hover">
                                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-retweet"></i></a></li>


                                                        <!-- làm ơn đừng xóa thằng này nữa -->
                                                        <li onclick="onClickAddToCart(${item.pid}, 1)"><a><i class="fa fa-shopping-cart"></i></a></li>
                                                    </ul>
                                                </div>
                                                <div class="product__item__text">
                                                    <h6><a href="productDetail?pid=${item.pid}">${item.pName}</a></h6>
                                                    <h5>${helpers.formatInt(item.price)} VNĐ</h5>
                                                </div>
                                            </div>
                                        </div>
                                        </c:if>
                                    </c:forEach>
                                </div>
                                <div class="product__pagination">
                                    <c:if test="${page > 1}">
                                        <input hidden type="radio" name="page" value="${page - 1}" id="page_previous" onchange="this.form.submit()">
                                        <label for="page_previous"><i class="fa fa-long-arrow-left"></i></label>
                                        </c:if>
                                        <c:forEach begin="${1}" end="${numberOfPage}" var="i">
                                        <input hidden type="radio" name="page" value="${i}" id="page_${i}" onchange="this.form.submit()">
                                        <label for="page_${i}" class="${page == i ? 'active' : ''}">${i}</label>
                                    </c:forEach>
                                    <c:if test="${page < numberOfPage }">
                                        <input hidden type="radio" name="page" value="${page + 1}" id="page_next" onchange="this.form.submit()">
                                        <label for="page_next"><i class="fa fa-long-arrow-right"></i></label>
                                        </c:if>
                                </div>
                            </c:if>


                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- Product Section End -->

        <!-- Footer Section Begin -->
         <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="home"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul>
                                <li>Của hàng HCM: 66 Nguyễn Trí Sách, Phường 15 Tân Bình</li>
                                <li>Cửa hàng HN: 63 Yên Lãng - Đống Đa - Hà Nội</li>
                                <li>Phone: 0123456789</li>
                                <li>Email: shophoaqua@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Js Plugins -->
      <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
<!--        <script src="assets/js/jquery.nice-select.min.js"></script>-->
        <script src="assets/js/jquery-ui.min.js"></script>
        <script src="assets/js/jquery.slicknav.js"></script>
        <script src="assets/js/mixitup.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/main.js"></script>

        <script>
                                        function onClickAddToCart(idItem, quantity) {
                                            console.log(idItem);
                                            if (quantity === 0) {
                                                event.preventDefault();
                                                alert("We apologize for this inconvenience." +
                                                        "\nThe item is currently out of stock, please comeback later");
                                            } else {
                                                let href = '/SWP391_FA23_SE1736_TEAM1/AddorCheckRedirectController?productId=' + idItem + '&quantity=1&path=shop';
                                                window.location.href = href;
                                            }
                                        }
        </script>

        <script>
            // Sử dụng JavaScript hoặc jQuery để tự động đóng alert sau một khoảng thời gian
            setTimeout(function () {
                $("#myAlert").alert('close'); // Sử dụng jQuery
                // Hoặc sử dụng JavaScript thuần
                // document.getElementById('myAlert').style.display = 'none';
            }, 3000); // 3000 milliseconds tương ứng với 3 giây
        </script>
        
        


    </body>

</html>