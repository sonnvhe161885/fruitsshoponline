<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean class="utils.Helpers" id="helpers"/>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Danh sách Orders | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">


    </head>


    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="home"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <c:if test="${message != null}">
            <c:if test="${alert.equals('Success')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-success alert-dismissible fade show text-center position-absolute " role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${alert.equals('Error')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-danger alert-dismissible fade show text-center position-absolute " role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${alert.equals('Warning')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-warning alert-dismissible fade show text-center position-absolute " role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
        </c:if>
        <!-- Sidebar menu-->
        <jsp:include page="navbar.jsp" flush="true" >
            <jsp:param name="orders" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="app-title">
                        <ul class="app-breadcrumb breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><b>Danh sách Orders</b></a></li>
                        </ul>
                        <div id="clock"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 alert-s">
                    <div class="tile">
                        <div class="tile-body">
                            <div class="row element-button">
                                <div class="col-sm-2">
                                    <c:if test="${sessionScope.account.role.roleName != 'Admin'}">
                                        <a class="btn btn-add btn-sm" href="${pageContext.request.contextPath}/manager-orders-seller" title="Thêm">
                                            Đơn hàng của tôi
                                        </a>
                                    </c:if>
                                </div>
                                <c:if test="${sessionScope.account.role.roleName == 'Admin'}">
                                    <div class="d-flex" style="align-items: center; gap: 8px">
                                        <div>Lọc trạng thái đơn hàng</div>
                                        <div>
                                            <select style = "padding: 8px" name="status" id='filter-status'>
                                                <option value="">Trạng thái</option>
                                                <c:forEach items="${statusFilter}" var="item">
                                                    <option value="${item.stName}" class="d-block flex-column">
                                                        ${item.stNameVi}
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>                
                                    </div>
                                </c:if>
                            </div>
                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <!--<th width="10"><input type="checkbox" id="all"></th>-->
                                        <th>Mã đơn hàng</th>
                                        <th>Tên khách hàng</th>
                                        <th>Số Điện thoại</th>
                                        <th>Địa Chỉ</th>
                                        <th>Ngày lập hóa đơn</th>

                                        <c:if test="${sessionScope.account.role.roleName == 'Admin'}">
                                            <th>Ngày giao hàng dự kiến</th>
                                            </c:if>
                                        <th>Trạng thái hóa đơn</th>
                                        <th>Tổng tiền</th>
                                            <c:if test="${sessionScope.account.role.roleName == 'Admin'}">
                                            <th>Seller</th>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role.roleName != 'Admin'}">
                                            <th>Chức Năng</th>
                                            </c:if>

                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${orders}" var="item">
                                        <tr>
                                            <!--<td width="10"><input type="checkbox" name="check1" value="1"></td>-->
                                            <td>${item.orderCode}</td>
                                            <td>${item.name}</td>
                                            <td>${item.phone}</td>
                                            <c:if test="${item.address != null}">
                                                <td>${item.address.address}, ${item.address.commune}, ${item.address.district}, ${item.address.city}</td>
                                            </c:if>
                                            <c:if test="${item.address == null}">
                                                <td>--</td>
                                            </c:if>
                                            <td>${item.createDate}</td>

                                            <c:if test="${sessionScope.account.role.roleName == 'Admin'}">
                                                <td>${item.deliveryDate}</td>
                                            </c:if>
                                            <c:if test="${item.statusModel != null}">
                                                <c:if test="${item.statusModel.stName == 'Delivery_Success'}">
                                                    <td><span class="badge bg-success">${item.statusModel.stNameVi}</span></td>
                                                    </c:if>
                                                    <c:if test="${item.statusModel.stName == 'Order_Cancel'}">
                                                    <td><span class="badge bg-danger">${item.statusModel.stNameVi}</span></td>
                                                    </c:if>
                                                    <c:if test="${item.statusModel.stName != 'Order_Cancel' && item.statusModel.stName != 'Delivery_Success'}">
                                                    <td><span class="badge bg-primary">${item.statusModel.stNameVi}</span></td>
                                                    </c:if>
                                                </c:if>
                                                <c:if test="${item.statusModel == null}">
                                                <td><span >--</span></td>
                                            </c:if>
                                            <td>${helpers.formatInt(item.totalPrice)} VNĐ</td>
                                            <c:if test="${sessionScope.account.role.roleName == 'Admin'}">
                                                <td>${item.seller.userName}</td>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role.roleName != 'Admin' && item.statusModel.stName != 'Order_Cancel'}">
                                                <td>
                                                    <div class="row m-1">
                                                        <button class="btn btn-primary btn-sm edit mr-2" type="button" title="Cập nhật trạng thái đơn hàng" id="show-emp"
                                                                data-toggle="modal" data-target="#ModalUP-${item.orderCode}"><i class="fas fa-edit"></i>
                                                        </button>
                                                        <button class="btn btn-primary btn-sm alert" type="button" title="Xem chi tiết đơn hàng" onclick="showDetail('${pageContext.request.contextPath}',${item.orderCode})"><i class="fas fa-eye"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </c:if>
                                            <c:if test="${sessionScope.account.role.roleName != 'Admin' && item.statusModel.stName == 'Order_Cancel'}">
                                                <td></td>
                                            </c:if>
                                        </tr>

                                        <!--
                                        MODAL
                                        -->
                                    <div class="modal fade" id="ModalUP-${item.orderCode}" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
                                         data-keyboard="false">
                                        <form action="${pageContext.request.contextPath}/manager-orders/update-status" method="POST">
                                            <input hidden="true" value="${item.orderCode}" name="orderCode"/>
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">

                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="form-group  col-md-12">
                                                                <span class="thong-tin-thanh-toan">
                                                                    <h5>Cập nhật thông tin trạng thái đơn hàng</h5>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <p>Mã đơn hàng: ${item.orderCode}</p>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <p>Tổng tiền: ${helpers.formatInt(item.totalPrice)} VNĐ</p>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <p>Họ tên: ${item.name}</p>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <p>Phone: ${item.phone}</p>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <c:if test="${item.addressId != null}">
                                                                    <p>Địa chỉ: ${item.address.address}, ${item.address.commune}, ${item.address.district}, ${item.address.city}</p>
                                                                </c:if>
                                                                <c:if test="${item.addressId == null}">
                                                                    <p>Địa chỉ: --</p>
                                                                </c:if>
                                                            </div>
                                                            <div class="form-group  col-md-6">
                                                                <label for="exampleSelect1" class="control-label">Trạng thái</label>
                                                                <select class="form-control" id="exampleSelect1" name="statusId">
                                                                    <c:if test="${item.status != null}}">
                                                                        <option value="${item.status}">${item.statusModel.stNameVi}</option>
                                                                    </c:if>
                                                                    <c:if test="${item.status != null}}">
                                                                        <option value="">Chọn trạng thái</option>
                                                                    </c:if>
                                                                    <c:forEach items="${statuses}" var="status">
                                                                        <option value = ${status.stid}>${status.stNameVi}</option>
                                                                    </c:forEach>

                                                                </select>
                                                            </div>
                                                            <div class="form-group  col-md-6">
                                                                <label for="deliveryDate" class="control-label">Ngày giao hàng dự kiến</label>
                                                                <input id="deliveryDate" name="deliveryDate" class="form-control" type="date" required="true"/>
                                                            </div>
                                                        </div>
                                                        <BR>
                                                        <button class="btn " style="background-color: #b1ffb5" type="submit">Lưu lại</button>
                                                        <a class="btn btn-cancel" data-dismiss="modal" href="#">Hủy bỏ</a>
                                                        <BR>
                                                    </div>
                                                    <div class="modal-footer">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!--
                                    MODAL
                                    -->
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
    <!--
    MODAL
    -->

    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="src/jquery.table2excel.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <!-- Data table plugin-->
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <!--     Js Plugins 
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>-->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
    <script src="js/app.js"></script>
    <script type="text/javascript">
                                                            $('#sampleTable').DataTable();
                                                            //Thời Gian
                                                            function time() {
                                                                var today = new Date();
                                                                var weekday = new Array(7);
                                                                weekday[0] = "Chủ Nhật";
                                                                weekday[1] = "Thứ Hai";
                                                                weekday[2] = "Thứ Ba";
                                                                weekday[3] = "Thứ Tư";
                                                                weekday[4] = "Thứ Năm";
                                                                weekday[5] = "Thứ Sáu";
                                                                weekday[6] = "Thứ Bảy";
                                                                var day = weekday[today.getDay()];
                                                                var dd = today.getDate();
                                                                var mm = today.getMonth() + 1;
                                                                var yyyy = today.getFullYear();
                                                                var h = today.getHours();
                                                                var m = today.getMinutes();
                                                                var s = today.getSeconds();
                                                                m = checkTime(m);
                                                                s = checkTime(s);
                                                                nowTime = h + " giờ " + m + " phút " + s + " giây";
                                                                if (dd < 10) {
                                                                    dd = '0' + dd
                                                                }
                                                                if (mm < 10) {
                                                                    mm = '0' + mm
                                                                }
                                                                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                                                                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                                                                        '</span>';
                                                                document.getElementById("clock").innerHTML = tmp;
                                                                clocktime = setTimeout("time()", "1000", "Javascript");

                                                                function checkTime(i) {
                                                                    if (i < 10) {
                                                                        i = "0" + i;
                                                                    }
                                                                    return i;
                                                                }
                                                            }
    </script>
    <script>
        function deleteRow(r) {
            var i = r.parentNode.parentNode.rowIndex;
            console.log(i);
            document.getElementById("myTable").deleteRow(i);
        }
        jQuery(function () {
            jQuery(".trash").click(function (e) {
                swal({
                    title: "Cảnh báo",
                    text: "Bạn có chắc chắn là muốn xóa sản phẩm này?",
                    buttons: ["Hủy bỏ", "Đồng ý"],
                })
                        .then((willDelete) => {
                            console.log(willDelete);
                            if (willDelete) {
                                swal("Đã xóa thành công.!", {

                                });
                            }
                        });
            });
        });
        oTable = $('#sampleTable').dataTable();
        $('#all').click(function (e) {
            $('#sampleTable tbody :checkbox').prop('checked', $(this).is(':checked'));
            e.stopImmediatePropagation();
        });
        function showModalUpdateStatus(orderCode) {
            console.log(orderCode)
            $("#ModalUP-" + orderCode).modal({backdrop: false, keyboard: false});
        }
    </script>
    <script>
        // Sử dụng JavaScript hoặc jQuery để tự động đóng alert sau một khoảng thời gian
        setTimeout(function () {
            $("#myAlert").alert('close'); // Sử dụng jQuery
            // Hoặc sử dụng JavaScript thuần
            // document.getElementById('myAlert').style.display = 'none';
        }, 3000); // 3000 milliseconds tương ứng với 3 giây
    </script>
    <script>
        var filter = document.getElementById("filter-status");
        filter.onchange = function (e) {
            console.log(e.target.value);
            console.log(window.location.href);
            let status = e.target.value;
            window.location.href = "manager-orders?status=" + status;
        };

        function showDetail(contexPath, orderCode) {
            console.log(orderCode, contexPath);
            window.location.href = contexPath + "/order-detail?action=update&orderCode=" + orderCode;
        }
    </script>

</html>s