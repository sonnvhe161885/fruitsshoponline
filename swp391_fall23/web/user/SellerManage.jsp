<%-- 
    Document   : UserManage
    Created on : Sep 18, 2023, 8:44:34 PM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<jsp:useBean class="dal.StoreDAO" id="sdao"/>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Danh sách Nhân Viên | Quản trị Admin</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="home"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../navbar.jsp" flush="true" >
            <jsp:param name="seller" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Danh sách nhân viên</b></a></li>
                </ul>

                <div id="clock"></div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">

                            <div class="row element-button">
                                <div class="col-sm-2">
                                    <a class="btn btn-add btn-sm" href="seller" title="Thêm"><i
                                            class="fas fa-list-ul"></i>
                                        Danh sách nhân viên</a>
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-add btn-sm" href="customer" title="Thêm"><i
                                            class="fas fa-list-ul"></i>
                                        Danh sách khách hàng</a>
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-add btn-sm" href="addseller" title="Thêm"><i
                                            class="fas fa-plus"></i>
                                        Thêm mới người dùng</a>
                                </div>
                            </div>
                            <table class="table table-hover table-bordered js-copytextarea" cellpadding="0" cellspacing="0" border="0"
                                   id="sampleTable">
                                <thead>
                                    <tr>
                                        <th width="10"><input type="checkbox" id="all"></th>
                                        <th width="15">ID</th>
                                        <th width="130">Họ và tên</th>
                                        <th>Email</th>
                                        <th>SĐT</th>
                                        <th>Cửa hàng</th>
                                        <th>Trạng thái</th>
                                        <th width="80">Tính năng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${user}" var = "x"> 
                                        <tr>
                                            <td width="10"><input type="checkbox" name="check1" value="1"></td>
                                            <td>${x.userId}</td>
                                            <td>${x.userName}</td>
                                            <td>${x.email}</td>
                                            <td>${x.phone}</td>
                                            <td>${x.store.stoName}</td>
                                            <td><span
                                                    class="badge ${x.status == 'true' ? 'bg-success' : 'bg-danger' }">${x.status == 'true' ? 'Hoạt động' : 'Khóa'}</span>
                                            </td>
                                            <td>
                                                <form action="sellerdetails" method="GET">
                                                <button class="btn btn-primary btn-sm edit" type="button" title="Sửa" id="show-emp" data-toggle="modal" data-target="#edit_product_${x.userId}">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                    <input type="hidden" name="userId" value="${x.userId}">
                                                    <button class="btn btn-primary btn-sm edit" type="submit" title="Xem chi tiết">
                                                        <i class="fas fa-eye"></i>
                                                    </button>
                                                </form>
                                            
                                        </tr><!-- comment -->
                                    <div class="modal fade" id="edit_product_${x.userId}" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
                                         data-keyboard="false">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <form action="updateseller" method="POST">
                                                        <input type="text" name="action" value="" hidden>
                                                        <div class="row">
                                                            <div class="form-group  col-md-12">
                                                                <span class="thong-tin-thanh-toan">
                                                                    <h5>Chỉnh sửa thông tin</h5>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">Id </label>
                                                                <input class="form-control" type="text" name="id" readonly value="${x.userId}">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">Tên</label>
                                                                <input class="form-control" type="text" name="name" required value="${x.userName}">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">Số điện thoại</label>
                                                                <input class="form-control" type="text" name="phone" required value="${x.phone}">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">Trạng thái</label>
                                                                <select class="form-control"
                                                                        id="exampleSelect1" name="status">
                                                                    <c:choose>
                                                                        <c:when test="${x.status == 'true'}">
                                                                            <option value="true" selected>Hoạt động</option>
                                                                            <option value="false">Khóa</option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <option value="false" selected>Khóa</option>
                                                                            <option value="true">Hoạt động</option>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <BR>
                                                        <BR>
                                                        <BR>
                                                        <button class="btn btn-save" type="submit">Lưu lại</button>
                                                        <a class="btn btn-cancel" data-dismiss="modal" href="#">Hủy bỏ</a>
                                                        <BR>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </c:forEach>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>



        <!-- Essential javascripts for application to work-->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="src/jquery.table2excel.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <!-- Data table plugin-->
        <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">$('#sampleTable').DataTable();</script>
        <script>

            function confirmDelete(button) {
                // Lấy form cha của button được nhấn
                var form = button.closest('.deleteForm');

                // Lấy giá trị userId từ form
                var userId = form.querySelector('.userId').value;

                if (confirm('Bạn có chắc chắn muốn xóa người dùng có id: ' + userId + ' không?')) {
                    // Gán giá trị userId vào form và submit
                    form.submit();
                }
            }
            //    function deleteRow(r) {
            //      var i = r.parentNode.parentNode.rowIndex;
            //      document.getElementById("myTable").deleteRow(i);
            //    }
            //    jQuery(function () {
            //      jQuery(".trash").click(function () {
            //        swal({
            //          title: "Cảnh báo",
            //         
            //          text: "Bạn có chắc chắn là muốn xóa nhân viên này?",
            //          buttons: ["Hủy bỏ", "Đồng ý"],
            //        })
            //          .then((willDelete) => {
            //            if (willDelete) {
            //              swal("Đã xóa thành công.!", {
            //                
            //              });
            //            }
            //          });
            //      });
            //    });
            //    oTable = $('#sampleTable').dataTable();
            //    $('#all').click(function (e) {
            //      $('#sampleTable tbody :checkbox').prop('checked', $(this).is(':checked'));
            //      e.stopImmediatePropagation();
            //    });

            //EXCEL
            // $(document).ready(function () {
            //   $('#').DataTable({

            //     dom: 'Bfrtip',
            //     "buttons": [
            //       'excel'
            //     ]
            //   });
            // });


            //Thời Gian
            function time() {
                var today = new Date();
                var weekday = new Array(7);
                weekday[0] = "Chủ Nhật";
                weekday[1] = "Thứ Hai";
                weekday[2] = "Thứ Ba";
                weekday[3] = "Thứ Tư";
                weekday[4] = "Thứ Năm";
                weekday[5] = "Thứ Sáu";
                weekday[6] = "Thứ Bảy";
                var day = weekday[today.getDay()];
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                nowTime = h + " giờ " + m + " phút " + s + " giây";
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                        '</span>';
                document.getElementById("clock").innerHTML = tmp;
                clocktime = setTimeout("time()", "1000", "Javascript");

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
            }
            //In dữ liệu
            var myApp = new function () {
                this.printTable = function () {
                    var tab = document.getElementById('sampleTable');
                    var win = window.open('', '', 'height=700,width=700');
                    win.document.write(tab.outerHTML);
                    win.document.close();
                    win.print();
                }
            }
            //     //Sao chép dữ liệu
            //     var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

            // copyTextareaBtn.addEventListener('click', function(event) {
            //   var copyTextarea = document.querySelector('.js-copytextarea');
            //   copyTextarea.focus();
            //   copyTextarea.select();

            //   try {
            //     var successful = document.execCommand('copy');
            //     var msg = successful ? 'successful' : 'unsuccessful';
            //     console.log('Copying text command was ' + msg);
            //   } catch (err) {
            //     console.log('Oops, unable to copy');
            //   }
            // });


            //Modal
            $("#show-emp").on("click", function () {
                $("#ModalUP").modal({backdrop: false, keyboard: false})
            });
        </script>
    </body>

</html>
