<%-- 
    Document   : index
    Created on : Sep 16, 2023, 10:29:09 PM
    Author     : kien
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Thêm Bài Ðăng | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxiconsa.min.css">
        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <script src="https://cdn.ckeditor.com/4.21.0/standard/ckeditor.js"></script>
    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">
                <!-- User Menu-->
                <li><a class="app-nav__item" href="home"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../navbar.jsp" flush="true" >
            <jsp:param name="post" value="active" />
        </jsp:include>
        <main class="app-content">

            <div class="row">
                <div class="col-md-12">
                    <div class="app-title">
                        <ul class="app-breadcrumb breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><b>Bảng điều khiển</b></a></li>
                        </ul>
                        <div id="clock"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--Left-->
                <div class="col-md-12">
                    <div class="tile">
                        <h3 class="tile-title">Tạo mới bài viết </h3>

                        <div class="tile-body">
                            <form class="row" method="post" action="AddBlog">

                                <div class="form-group col-md-6">
                                    <input class="form-control" type="hidden" name="bid" value="" placeholder="">
                                    <label class="control-label">Tiêu đề</label>
                                    <input class="form-control" value="${b.blogTitle}" name="title" type="text">
                                </div>
                               
                                <div class="form-group col-md-6 ">
                                    <label for="exampleSelect1" class="control-label">Trạng thái</label>
                                    <select  name="status" class="form-control" id="exampleSelect1">
                                        <option value="1" ${b.stid==1?"selected":""}>Hiện</option>
                                        <option value="2" ${b.stid==2?"selected":""}>Ẩn</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 ">
                                    <label for="exampleSelect1" class="control-label">Danh mục</label>
                                    <select name="cate" class="form-control" id="exampleSelect1">
                                        <c:forEach var="c" items="${cate}">
                                            <option value="${c.categoryBlogId}" ${b.categoryBlogId==c.categoryBlogId?"selected":""}>${c.categoryBlogName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">Nội dung:</label>
                                    <textarea class="form-control" name="Description" id="editoradd" maxlength="300" style="height: 155px;">${b.blogDetail}</textarea>
                                </div>
                                <div class="form-group col-md-10">
                                    <label class="control-label" >Ảnh</label>

                                    <input class="form-control" id="imgadd" required onchange="changeimgadd()" name="image" type="file" >
                                    <input name="proimage" id="imageadd" value="" type="hidden" >
                                    <image  src="${b.thumbnail}" id="demoimgadd" style="margin-top: 5px;" width="30%">
                                </div>

                        </div>
                        <div class="form-group col-md-6 ">
                            <button class="btn btn-save" type="submit" onclick="return confirm('Bạn có muốn thêm mới bài viết này không?')">Thêm mới</button>
                            <a class="btn btn-cancel" href="BlogList" onclick="return confirm('Sau khi hủy những thay đổi sẽ không được lưu lại!')">Hủy bỏ</a>
                        </div>
                        </form>
                    </div>
                    <!--END left-->

                </div>


        </main>
        <script src="js/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="js/popper.min.js"></script>
        <script src="https://unpkg.com/boxicons@latest/dist/boxicons.js"></script>
        <!--===============================================================================================-->
        <script src="js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>
        <!--===============================================================================================-->
        <script src="js/plugins/pace.min.js"></script>
        <!--===============================================================================================-->
        <script type="text/javascript" src="js/plugins/chart.js"></script>
        <!--===============================================================================================-->

        <script type="text/javascript">
                                        //Thời Gian
                                        function time() {
                                            var today = new Date();
                                            var weekday = new Array(7);
                                            weekday[0] = "Chủ Nhật";
                                            weekday[1] = "Thứ Hai";
                                            weekday[2] = "Thứ Ba";
                                            weekday[3] = "Thứ Tư";
                                            weekday[4] = "Thứ Năm";
                                            weekday[5] = "Thứ Sáu";
                                            weekday[6] = "Thứ Bảy";
                                            var day = weekday[today.getDay()];
                                            var dd = today.getDate();
                                            var mm = today.getMonth() + 1;
                                            var yyyy = today.getFullYear();
                                            var h = today.getHours();
                                            var m = today.getMinutes();
                                            var s = today.getSeconds();
                                            m = checkTime(m);
                                            s = checkTime(s);
                                            nowTime = h + " giờ " + m + " phút " + s + " giây";
                                            if (dd < 10) {
                                                dd = '0' + dd
                                            }
                                            if (mm < 10) {
                                                mm = '0' + mm
                                            }
                                            today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                                            tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                                                    '</span>';
                                            document.getElementById("clock").innerHTML = tmp;
                                            clocktime = setTimeout("time()", "1000", "Javascript");

                                            function checkTime(i) {
                                                if (i < 10) {
                                                    i = "0" + i;
                                                }
                                                return i;
                                            }
                                        }
        </script>
        <script>
            CKEDITOR.replace('editoradd');
        </script>
        <script>
            function changeimgadd() {
                var file = document.getElementById("imgadd").files[0];
                if (file.name.match(/.+\.(jpg|png|jpeg)/i)) {
                    if (file.size / (1024 * 1024) < 5) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                            document.getElementById("imageadd").value = (fileReader.result);
                            document.getElementById("demoimgadd").src = (fileReader.result);
                        }
                    } else {
                        uploadError();
                    }
                } else {
                    uploadError();
                }
            }
            function uploadError() {
                alert('Please upload photo file < 5MB')
                document.getElementById("imgadd").files[0].value = ''
                document.getElementById("imgadd").type = '';
                document.getElementById("imgadd").type = 'file';
            }

        </script>
    </div><!-- ./wrapper -->
</body>

</html>