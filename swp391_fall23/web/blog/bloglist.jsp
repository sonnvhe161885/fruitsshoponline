<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Danh sách Bài Ðăng | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <c:if test="${param['index']==null }">   
            <c:set var = "index" scope = "page" value = "1"/>
        </c:if>
        <c:if test="${param['index']!=null}">
            <c:set var = "index" scope = "page" value = "${param['index']}"/>
        </c:if>
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="home"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../navbar.jsp" flush="true" >
            <jsp:param name="post" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Danh sách bài viết</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">
                            <div class="row element-button">
                                <div class="col-sm-2">

                                    <a class="btn btn-add btn-sm" href="AddBlog" title="Thêm"><i class="fas fa-plus"></i>
                                        Tạo mới Bài viết</a>
                                </div>


                                <div class="col-sm-8">
                                    <form style="display: flex; justify-content: space-around; align-items: center; width: 120%;" method="get" action="BlogList">
                                        <label for="cid" style="margin-right: 10px; white-space: nowrap;"> Thể loại:</label> <select name="cid" class="form-control">
                                            <option value="">Tất cả</option>
                                            <c:forEach var="c" items="${cate}">
                                                <option value="${c.categoryBlogId}" ${param['cid']==c.categoryBlogId?"selected":""}>${c.categoryBlogName}</option>
                                            </c:forEach>
                                        </select>
                                        <label for="status" style="margin-right: 10px;white-space: nowrap;"> Trạng thái:</label> <select class="form-control" name="status">
                                            <option value="" ${param['status']==""?"selected":""}>Tất cả</option>     
                                            <option value="1" ${param['status']=="1"?"selected":""}>Hiện</option>
                                            <option value="2" ${param['status']=="0"?"selected":""}>Ẩn</option>
                                        </select>
                                        <label for="sort" style="margin-right: 10px;white-space: nowrap;">Sắp sếp: </label>
                                        <select name="sort" class="form-control">
                                            <option value="" ${param['sort']==""?"selected":""}>Mặc định</option>     
                                            <option value="order by b.blogDate desc" ${param['sort']=="order by b.blogDate desc"?"selected":""}>Mới</option>     
                                            <option value="order by b.blogDate asc" ${param['sort']=="order by b.blogDate asc"?"selected":""}>Cũ</option>     
                                        </select>
                                        <label for="cid" style="margin-right: 10px;white-space: nowrap;"> Tìm kiếm:</label> <input style="margin-right: 5px;" type="text" maxlength="50" value="${param['search']}" name="search" class="form-control">
                                        <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                                    </form>
                                </div>
                            </div>

                            <table class="table table-hover table-bordered" >
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tên</th>
                                        <th>Danh mục</th>
                                        <th>Ngày tạo</th>
                                        <th>Ảnh</th>
                                        <th>Trạng thái</th>
                                        <th>Thay đổi trạng thái</th>
                                        <th>Xóa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="s" items="${prolist}">
                                        <tr>
                                            <td><a  href="./BlogDetail?bid=${s.blogId}">${s.blogId}</a></td>
                                            <td><a  href="./BlogDetail?bid=${s.blogId}">${s.blogTitle}</a></td>
                                            <td>${s.category}</td>
                                            <td>${s.blogDate}</td>    
                                            <td><img src="${s.thumbnail}" width="200px" alt="alt"/></td>
                                                <c:if test="${s.stid==1}">
                                                <td><button class="badge bg-success">Hiện</button></td>    
                                            </c:if>
                                            <c:if test="${s.stid==2}">
                                                <td><button class="badge bg-danger">Ẩn</button></td>    
                                            </c:if>
                                            <c:if test="${s.stid==2}">
                                                <td><a class="btn btn-success" href="ChangeBlogStatus?ss=1&bid=${s.blogId}">Ẩn</a></td>    
                                            </c:if>
                                            <c:if test="${s.stid==1}">
                                                <td><a class="btn btn-danger" href="ChangeBlogStatus?ss=2&bid=${s.blogId}">Hiện</a></td>    
                                            </c:if>
                                            <td><a class="btn btn-primary btn-sm trash" href="./ChangeBlogStatus?bid=${s.blogId}" title="Xóa" onclick="return confirm('Bạn có muốn xóa bài viết này không?')"><i class="fas fa-trash-alt"></i> </a>
                                                <!--<button class="btn btn-primary btn-sm edit" type="button" title="Sửa"><i class="fa fa-edit"></i></button></td>-->
                                        </tr>
                                    </tbody>
                                </c:forEach>

                            </table>
                            <div class="pagination-arena " style="margin-left: 40%">
                                <ul class="pagination">
                                    <li class="page-item"><a href="./BlogList?cid=${param['cid']}&sort=${param['sort']}&status=${param['status']}&search=${param['search']}&type-search=${param['type-search']}&index=1" class="page-link"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                                    <li class="page-item">
                                        <a href="./BlogList?cid=${param['cid']}&sort=${param['sort']}&status=${param['status']}&search=${param['search']}&type-search=${param['type-search']}&index=${index-2}" class="page-link " style="${index-2<1?"display:none;":""}">${index-2}</a></li>
                                    <li class="page-item">
                                        <a href="./BlogList?cid=${param['cid']}&sort=${param['sort']}&status=${param['status']}&search=${param['search']}&type-search=${param['type-search']}&index=${index-1}" class="page-link " style="${index-1<1?"display:none;":""}">${index-1}</a></li>
                                    <li class="page-item active">
                                        <a href="./BlogList?cid=${param['cid']}&sort=${param['sort']}&status=${param['status']}&search=${param['search']}&type-search=${param['type-search']}&index=${index}" class="page-link">${index}</a></li>
                                    <li class="page-item">
                                        <a href="./BlogList?cid=${param['cid']}&sort=${param['sort']}&status=${param['status']}&search=${param['search']}&type-search=${param['type-search']}&index=${index+1}" class="page-link " style="${index+1>numberPage?"display:none;":""}" >${index+1}</a></li>
                                    <li class="page-item">
                                        <a href="./BlogList?cid=${param['cid']}&sort=${param['sort']}&status=${param['status']}&search=${param['search']}&type-search=${param['type-search']}&index=${index+2}" class="page-link " style="${index+2>numberPage?"display:none;":""}">${index+2}</a></li>
                                    <li><a href="./BlogList?cid=${param['cid']}&sort=${param['sort']}&status=${param['status']}&search=${param['search']}&type-search=${param['type-search']}&index=${numberPage}" class="page-link"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- Essential javascripts for application to work-->
        <script src="./js/jquery-3.2.1.min.js"></script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="src/jquery.table2excel.js"></script>
        <script src="./js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="./js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    </body>
</html>