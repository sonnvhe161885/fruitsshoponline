<%-- 
    Document   : Register
    Created on : Oct 2, 2023, 8:16:11 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<link rel="stylesheet" type="text/css" href="css/login.css">
<div class="container" id="container">
	<div class="form-container sign-in-container">
            <form action="forgot" method="post">
			<h1>Quên mật khẩu</h1>
                        <input type="hidden" name="redirectPage" value="${page}" />
			<input type="email" name="email" placeholder="Email*" required/>
                        <button type="submit">Khôi phục</button>
		</form>
            <div class="message" id="message">${msg}</div>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-right">
				<button class="ghost" id="signUp">Đăng Ký</button>
                                <button class="ghost" id="signIn">Đăng Nhập</button>
			</div>
		</div>
	</div>
</div>
        </div>
        <div class="back-button">
        <a href="home" onclick="goToHomePage()">
        <span>&larr;</span> Trở về trang chủ
        </a>
</div>
</html>
<style>
  .back-button {
  margin: 20px;
}

.back-button a {
  text-decoration: none;
  font-size: 16px;
}

.back-button a span {
  margin-right: 7px;
}
.ghost{
    margin-bottom: 10px;
}
</style>

<script>
    const signUpButton = document.getElementById('signUp');
     const signInButton = document.getElementById('signIn');

    signUpButton.addEventListener('click', () => {
        // Chuyển hướng đến servlet khi nhấn nút "Sign Up"
        window.location.href = 'register';
    });
     signInButton.addEventListener('click', () => {
        // Chuyển hướng đến servlet khi nhấn nút "Sign Up"
        window.location.href = 'login';
    });
    
</script>


